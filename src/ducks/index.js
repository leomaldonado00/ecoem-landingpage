import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web and AsyncStorage for react-native
import createSagaMiddleware from 'redux-saga'; // saga
import i18nStore from './i18n';

const reducers = combineReducers({
  i18nStore,
});

const persistConfig = {
  key: 'Ecoem',
  storage,
};
const persistedReducer = persistReducer(persistConfig, reducers);
const sagaMiddleware = createSagaMiddleware(); // saga

export default () => {
  const store = createStore(
    persistedReducer,
    undefined,
    compose(applyMiddleware(sagaMiddleware)), // saga
  );
  const persistor = persistStore(store);
  return { store, persistor, sagaMiddleware };
};
