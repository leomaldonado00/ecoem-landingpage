import { REHYDRATE } from 'redux-persist';
import * as types from './types';
import en from '../../i18n/en';
import es from '../../i18n/es';
import langStrings from '../../i18n';

const initialState = {
  lang: langStrings.getLanguage() === 'en' ? en : es,
  langCode: langStrings.getLanguage(),
};

export default (state = initialState, { type, lang, payload }) => {
  let incoming;
  switch (type) {
    case types.SET_EN:
      return {
        lang,
        langCode: 'en',
      };
    case types.SET_ES:
      return {
        lang,
        langCode: 'es',
      };
    case REHYDRATE:
      incoming = payload && payload.i18nStore;
      if (incoming) {
        if (incoming.langCode) {
          langStrings.setLanguage(incoming.langCode);
        }
        return {
          ...incoming,
        };
      }

      return initialState;
    default:
      return state;
  }
};
