import reducers from './reducers';

export default reducers;

export { langCode } from './selector';
export { setEn, setEs } from './operations';
