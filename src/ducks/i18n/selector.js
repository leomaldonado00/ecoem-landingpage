import { get } from 'lodash';

export const langCode = state => get(state, 'i18nStore.langCode');
