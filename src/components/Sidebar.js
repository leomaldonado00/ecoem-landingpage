import React from 'react';
import { withRouter } from 'react-router';
import styled from 'styled-components';
// import { MdClear } from 'react-icons/md';

const SideBar = ({ history, itemHeader, showMenuTemp, changeRoute, item }) => {
  return (
    <SideBarWrap showMenuTemp={showMenuTemp}>
      <UlSide marginTop="30px">
        <LiHome
          onClick={() => {
            changeRoute('intro');
          }}
          item={item === 'home' ? 'home' : null}
        >
          Home
        </LiHome>

        <LiServices
          onClick={() => {
            history.push('/services');
          }}
          item={itemHeader === 'services' ? 'services' : null}
        >
          Servicios
        </LiServices>

        <LiPortfolio
          onClick={() => {
            changeRoute('portfolio');
            // history.push('/list-portfolio/1');
          }}
          item={item === 'portfolio' ? 'portfolio' : null}
        >
          Portafolio
        </LiPortfolio>

        <LiContact
          onClick={() => {
            // history.push('/list-contact/1');
            changeRoute('contact');
          }}
          item={item === 'contact' ? 'contact' : null}
        >
          Contacto
        </LiContact>

        <LiBlog
          onClick={() => {
            // history.push('/list-blog/1');
            changeRoute('footer');
          }}
          item={item === 'blog' ? 'blog' : null}
        >
          Blog
        </LiBlog>
        <a href="https://www.instagram.com/ecoem.studio/" target="blank">
          <LiInsta
            item={item === 'instagram' ? 'instagram' : null}
          >
            Instagram
          </LiInsta>
        </a>
        <a href="https://twitter.com/ecoem_ve" target="blank">
            <LiTwiter
            item={item === 'twiter' ? 'twiter' : null}
            >
            Twiter
            </LiTwiter>
        </a>
      </UlSide>
    </SideBarWrap>
  );
};

const LiHome = styled.div`
  width: calc(100% - 25px);
  height: auto;
  position: ${props => props.position};
  bottom: ${props => props.bottom};
  font-family: source sans pro;
  font-size: 38px;

  color: ${props => (props.item === 'home' ? '#ffffff' : '#0047ba')};
  font-weight: ${props => (props.item === 'home' ? 'bold' : 'bold')};
  /* background-color: ${props =>
    props.item === 'home' ? '#00beb6' : 'none'}; */

    font-stretch: normal;
    font-style: normal;
    line-height: 1.58;
    letter-spacing: 0.38px;
    text-align: left;
  display: flex;
  justify-content: flex-start;
  padding-left: 25px;
  align-items: ${props => props.alignItems || 'center'};
  /*color: #0047ba;*/
  cursor: pointer;
  :hover {
    /* background-color: #00beb6; */
    color: #ffffff;
  };
`;
const LiTwiter = styled.div`
  width: calc(100% - 25px);
  height: auto;
  position: ${props => props.position};
  bottom: ${props => props.bottom};
  font-family: source sans pro;
  font-size: 38px;

  color: ${props => (props.item === 'twiter' ? '#ffffff' : '#0047ba')};
  font-weight: ${props => (props.item === 'twiter' ? 'bold' : 'bold')};
  /* background-color: ${props =>
    props.item === 'twiter' ? '#00beb6' : 'none'}; */

    font-stretch: normal;
    font-style: normal;
    line-height: 1.58;
    letter-spacing: 0.38px;
    text-align: left;
  display: flex;
  justify-content: flex-start;
  padding-left: 25px;
  align-items: ${props => props.alignItems || 'center'};
  /*color: #0047ba;*/
  cursor: pointer;
  :hover {
    /* background-color: #00beb6; */
    color: #ffffff;
  };
`;
const LiServices = styled.div`
width: calc(100% - 25px);
    height: auto;
    color: ${props => (props.item === 'services' ? '#ffffff' : '#0047ba')};
    font-weight: ${props => (props.item === 'services' ? 'bold' : 'bold')};
    /* background-color: ${props =>
      props.item === 'services' ? '#00beb6' : 'none'}; */
    position: ${props => props.position};
    bottom: ${props => props.bottom};
    font-family: source sans pro;
    font-size: 38px;
    /* font-weight: normal; */
    font-stretch: normal;
    font-style: normal;
    line-height: 1.58;
    letter-spacing: 0.38px;
    text-align: left;    
    display: flex;
    justify-content: flex-start;
    padding-left: 25px;
    align-items: ${props => props.alignItems || 'center'};
    /* color: #0047ba; */
    cursor: pointer;
    :hover {
      /* background-color: #00beb6; */
      color: #ffffff;
    };
`;
const LiBlog = styled.div`
    width: calc(100% - 25px);
    height: auto;
    color: ${props => (props.item === 'blog' ? '#ffffff' : '#0047ba')};
    font-weight: ${props => (props.item === 'blog' ? 'bold' : 'bold')};
    /* background-color: ${props =>
      props.item === 'blog' ? '#00beb6' : 'none'}; */
    position: ${props => props.position};
    bottom: ${props => props.bottom};
    font-family: source sans pro;
    font-size: 38px;
    /* font-weight: normal; */
    font-stretch: normal;
    font-style: normal;
    line-height: 1.58;
    letter-spacing: 0.38px;
    text-align: left;    
    display: flex;
    justify-content: flex-start;
    padding-left: 25px;
    align-items: ${props => props.alignItems || 'center'};
    /* color: #0047ba; */
    cursor: pointer;
    :hover {
      /* background-color: #00beb6; */
      color: #ffffff;
    };
`;
const LiPortfolio = styled.div`
  width: calc(100% - 25px);
  height: auto;
  color: ${props => (props.item === 'portfolio' ? '#ffffff' : '#0047ba')};
  font-weight: ${props => (props.item === 'portfolio' ? 'bold' : 'bold')};
  /* background-color: ${props =>
    props.item === 'portfolio' ? '#00beb6' : 'none'}; */
  position: ${props => props.position};
  bottom: ${props => props.bottom};
  font-family: source sans pro;
  font-size: 38px;
  /* font-weight: normal; */
    font-stretch: normal;
    font-style: normal;
    line-height: 1.58;
    letter-spacing: 0.38px;
    text-align: left;
  display: flex;
  justify-content: flex-start;
  padding-left: 25px;
  align-items: ${props => props.alignItems || 'center'};
  /* color: #0047ba; */
  cursor: pointer;
  :hover {
    /* background-color: #00beb6; */
    color: #ffffff;
  };
`;
const LiContact = styled.div`
  width: calc(100% - 25px);
  height: auto;
  color: ${props => (props.item === 'contact' ? '#ffffff' : '#0047ba')};
  font-weight: ${props => (props.item === 'contact' ? 'bold' : 'bold')};
  /* background-color: ${props =>
    props.item === 'contact' ? '#00beb6' : 'none'}; */
  position: ${props => props.position};
  bottom: ${props => props.bottom};
  font-family: source sans pro;
  font-size: 38px;
  /* font-weight: normal; */
    font-stretch: normal;
    font-style: normal;
    line-height: 1.58;
    letter-spacing: 0.38px;
    text-align: left;
  display: flex;
  justify-content: flex-start;
  padding-left: 25px;
  align-items: ${props => props.alignItems || 'center'};
  /* color: #0047ba; */
  cursor: pointer;
  :hover {
    /* background-color: #00beb6; */
    color: #ffffff;
  };
`;
const LiInsta = styled.div`
  width: calc(100% - 25px);
  height: auto;
  color: ${props => (props.item === 'instagram' ? '#ffffff' : '#0047ba')};
  font-weight: ${props => (props.item === 'instagram' ? 'bold' : 'bold')};
  /* background-color: ${props =>
    props.item === 'instagram' ? '#00beb6' : 'none'}; */
  position: ${props => props.position};
  bottom: ${props => props.bottom};
  font-family: source sans pro;
  font-size: 38px;
  /* font-weight: normal; */
    font-stretch: normal;
    font-style: normal;
    line-height: 1.58;
    letter-spacing: 0.38px;
    text-align: left;
  display: flex;
  justify-content: flex-start;
  padding-left: 25px;
  align-items: ${props => props.alignItems || 'center'};
  /* color: #0047ba; */
  cursor: pointer;
  :hover {
    /* background-color: #00beb6; */
    color: #ffffff;
  };
`;
const UlSide = styled.div`
  width: 100%;
  height: ${props => props.height}
  display: grid;
  margin-top: ${props => props.marginTop};
`;
const SideBarWrap = styled.div`
  display: none;
  @media (max-width: 992px) {
    display: block;
    top: 84px;
    background: #262626;
    width: 100vw;
    min-height: 100vh;
    z-index: 999;
    position: absolute;
    left: ${props => (props.showMenuTemp ? '0px' : '-110vw')};
    transition-duration: 0.3s;
    transition-timing-function: ease-out;
  }
`;

export default withRouter(SideBar);
