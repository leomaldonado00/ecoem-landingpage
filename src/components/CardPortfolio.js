import React from 'react'
import styled from 'styled-components';
import Text from './Text'
import Diver from './Diver'
import ImgGral from './ImgGral';

const CardPortfolio = (props) => {
    return (
        <Container
        margin="auto"
        width="248px"
        // minHeight="201px"
        height="auto"
        // backgroundColor="#F7F8FB"
            boxShadow=" 0px 6px 6px 0px gray"
        >

            <ImgGral width="248px" height="260px" src={props.srcImg} alt="portfolio" />

            <Diver marginTop="20px" textAlign="left">
                <Text 
                    fontSize="21px"
                    textAlign="left"
                    color="#000000"
                    fontWeight="bold"
                    lineHeight="1.14"
                    display="inline"
                    >
                    {props.title}
                </Text> 
                <Text 
                    fontSize="21px"
                    textAlign="left"
                    color="#000000"
                    fontWeight="normal"
                    lineHeight="1.14"
                    display="inline"
                    >
                    {props.description}
                </Text> 
                <a href={props.link} target="blank">
                    <Text 
                        fontSize="19px"
                        textAlign="left"
                        color="#000000"
                        fontWeight="600"
                        fontStyle="italic"
                        lineHeight="1.14"
                        display="block"
                        cursor="pointer"
                        >
                        {props.page}
                    </Text> 
                </a>
            </Diver>

            {/* <Diver marginLeft="15px" marginTop="5px" cursor="pointer" height="15px" width="15px" display="flex" justifyContent="center" alignItems="center">
                    <ImgNext src="/assets/images/nextdark.png" alt="next" />
                </Diver> */}

            
        </Container>
    );
};

/* const ImgNext = styled.img`
  width: 15px;
  height: 15px;
  `; */
const Container = styled.div`
margin: ${props => props.margin || "0"};
    padding-bottom: 5px;
    width: ${props => props.width};
    max-width: ${props => props.maxWidth};
    heigth: ${props => props.heigth}; 
    min-height: ${props => props.minHeight};
    background-color: ${props => props.backgroundColor || "#FFF"};
    /* box-shadow:${props => props.boxShadow}; */
    /* border-radius: 10px; */
    /* @media (max-width : 1000px) {
        max-width: 200px;
    }; */
    @media (max-width : 750px) {
        margin: auto;
    };
`;
/*
const ImgBackground = styled.img`
    width: ${props => props.width || '100%'};
    heigth: ${props => props.heigth || '100%'};
`;
*/

export default CardPortfolio;