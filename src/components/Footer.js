import React from 'react';
import Text from './Text';
import Diver from './Diver';
import ImgGral from './ImgGral';

const Footer = () => {
  return (
    <div id="footer">
        <Diver display="flex" flexDir="column" alignItems="center" width="100%" height="auto" backgroundColor="#262626">
            <Diver display="flex" flexDir10="column" justifyContent="space-between" alignItems10="flex-start" marginBottom="80px" width="100%" marginTop="50px">
                    <Diver marginLeft="80px" marginLeft10="10px">
                        <ImgGral width="285px" width92="209px" height="auto" src="/assets/images/ecoemlogo.png" alt="credo" />
                    </Diver>
                <Diver display="flex" flexDir10="column" marginLeft10="10px" marginRight="80px" marginRight10="0px" marginTop10="25px">
                    <Diver display="flex" flexDir="column" alignItems="center" alignItems10="flex-start" height="auto">
                        <Text fontSize="13px" fontSize10="22px" color="#0047ba" textAlign="right" lineHeight="1.45" letterSpacing="0.22">Contacto</Text>
                        <Text fontSize="13px" fontSize10="22px" color="#ffffff" textAlign="right" lineHeight="1.45" letterSpacing="0.22">hello@ecoem.co</Text>
                        <Text fontSize="13px" fontSize10="22px" color="#ffffff" textAlign="right" lineHeight="1.45" letterSpacing="0.22">ecoem.studio</Text>
                    </Diver>

                    {/* <Diver display="flex" flexDir="column" alignItems="center" alignItems10="flex-start" height="auto" marginLeft="100px" marginLeft10="0px">
                        <Text fontSize="13px" fontSize10="22px" color="#0047ba" textAlign="right" lineHeight="1.45" letterSpacing="0.22">Nuestra Oficina</Text>
                        <Text fontSize="13px" fontSize10="22px" color="#ffffff" textAlign="right" lineHeight="1.45" letterSpacing="0.22">Multicentro Empresarial</Text>
                        <Text fontSize="13px" fontSize10="22px" color="#ffffff" textAlign="right" lineHeight="1.45" letterSpacing="0.22">Los Palos Grandes, Piso 8</Text>
                        <Text fontSize="13px" fontSize10="22px" color="#ffffff" textAlign="right" lineHeight="1.45" letterSpacing="0.22">Caracas, Venezuela</Text>
                    </Diver> */}

                    <Diver display="flex" flexDir="column" alignItems="center" alignItems10="flex-start" height="auto" marginLeft="100px" marginLeft10="0px">
                        <Text fontSize="13px" fontSize10="22px" color="#0047ba" textAlign="right" lineHeight="1.45" letterSpacing="0.22">Síguenos</Text>
                        <a href="https://www.facebook.com/ecoem.ve" target="blank">
                            <Text fontSize="13px" fontSize10="22px" color="#ffffff" colorh="#0046B9" textAlign="right" lineHeight="1.45" letterSpacing="0.22" cursor="pointer">Facebook</Text>
                        </a>
                        <a href="https://www.instagram.com/ecoem.studio/" target="blank">
                            <Text fontSize="13px" fontSize10="22px" color="#ffffff" colorh="#0046B9" textAlign="right" lineHeight="1.45" letterSpacing="0.22" cursor="pointer">Instagram</Text>
                        </a>
                        <a href="https://twitter.com/ecoem_ve" target="blank">
                            <Text fontSize="13px" fontSize10="22px" color="#ffffff" colorh="#0046B9" textAlign="right" lineHeight="1.45" letterSpacing="0.22" cursor="pointer">Twitter</Text>
                        </a>
                    </Diver>
                </Diver>
            </Diver>
        </Diver>
    </div>
  );
};


export default Footer;
