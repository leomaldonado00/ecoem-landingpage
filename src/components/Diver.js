import React from 'react';
import styled from 'styled-components';

const Diver = props => (
  <DiverD
    fontWeight={props.fontWeight}
    color={props.color}
    textAlign={props.textAlign}
    width={props.width}
    height={props.height}
    marginTop={props.marginTop}
    marginRight={props.marginRight}
    marginBottom={props.marginBottom}
    marginLeft={props.marginLeft}
    paddingTop={props.paddingTop}
    paddingRight={props.paddingRight}
    paddingBottom={props.paddingBottom}
    paddingLeft={props.paddingLeft}
    display={props.display}
    justifyContent={props.justifyContent}
    alignItems={props.alignItems}
    alignSelf={props.alignSelf}
    flexDir={props.flexDir}
    gridColumns={props.gridColumns}
    gridGap={props.gridGap}
    justifyItems={props.justifyItems}
    justifySelf={props.justifySelf}
    position={props.position}
    top={props.top}
    right={props.right}
    bottom={props.bottom}
    left={props.left}
    minHeight={props.minHeight}
    minWidth={props.minWidth}
    zIndex={props.zIndex}
    backgroundColor={props.backgroundColor}
    backgroundImg={props.backgroundImg}
    backgroundSize={props.backgroundSize}
    backgroundPos={props.backgroundPos}
    backgroundRep={props.backgroundRep}
    maxWidth={props.maxWidth}
    maxHeight={props.maxHeight}
    objectFit={props.objectFit}
    border={props.border}
    borderRadius={props.borderRadius}
    boxShadow={props.boxShadow}
    overflowX={props.overflowX}
    overflowY={props.overflowY}
    textOF={props.textOF}
    cursor={props.cursor}
    opacity={props.opacity}
    backgroundColorh={props.backgroundColorh}
    borderh={props.borderh}
    borderRadiush={props.borderRadiush}
    paddingToph={props.paddingToph}
    paddingRighth={props.paddingRighth}
    paddingBottomh={props.paddingBottomh}
    paddingLefth={props.paddingLefth}
    width12={props.width12}
    height12={props.height12}
    marginTop12={props.marginTop12}
    marginRight12={props.marginRight12}
    marginBottom12={props.marginBottom12}
    marginLeft12={props.marginLeft12}
    paddingTop12={props.paddingTop12}
    paddingRight12={props.paddingRight12}
    paddingBottom12={props.paddingBottom12}
    paddingLeft12={props.paddingLeft12}
    display12={props.display12}
    justifyContent12={props.justifyContent12}
    alignItems12={props.alignItems12}
    gridColumns12={props.gridColumns12}
    gridGap12={props.gridGap12}
    justifyItems12={props.justifyItems12}
    justifySelf12={props.justifySelf12}
    position12={props.position12}
    top12={props.top12}
    right12={props.right12}
    bottom12={props.bottom12}
    left12={props.left12}
    minHeight12={props.minHeight12}
    minWidth12={props.minWidth12}
    zIndex12={props.zIndex12}
    backgroundColor12={props.backgroundColor12}
    backgroundImg12={props.backgroundImg12}
    backgroundSize12={props.backgroundSize12}
    backgroundPos12={props.backgroundPos12}
    backgroundRep12={props.backgroundRep12}
    maxWidth12={props.maxWidth12}
    maxHeight12={props.maxHeight12}
    objectFit12={props.objectFit12}
    borderRadius12={props.borderRadius12}
    boxShadow12={props.boxShadow12}
    overflowX12={props.overflowX12}
    overflowY12={props.overflowY12}
    width10={props.width10}
    height10={props.height10}
    marginTop10={props.marginTop10}
    marginRight10={props.marginRight10}
    marginBottom10={props.marginBottom10}
    marginLeft10={props.marginLeft10}
    paddingTop10={props.paddingTop10}
    paddingRight10={props.paddingRight10}
    paddingBottom10={props.paddingBottom10}
    paddingLeft10={props.paddingLeft10}
    display10={props.display10}
    flexDir10={props.flexDir10}
    justifyContent10={props.justifyContent10}
    alignItems10={props.alignItems10}
    gridColumns10={props.gridColumns10}
    gridGap10={props.gridGap10}
    justifyItems10={props.justifyItems10}
    justifySelf10={props.justifySelf10}
    position10={props.position10}
    top10={props.top10}
    right10={props.right10}
    bottom10={props.bottom10}
    left10={props.left10}
    minHeight10={props.minHeight10}
    minWidth10={props.minWidth10}
    zIndex10={props.zIndex10}
    backgroundColor10={props.backgroundColor10}
    backgroundImg10={props.backgroundImg10}
    backgroundSize10={props.backgroundSize10}
    backgroundPos10={props.backgroundPos10}
    backgroundRep10={props.backgroundRep10}
    maxWidth10={props.maxWidth10}
    maxHeight10={props.maxHeight10}
    objectFit10={props.objectFit10}
    borderRadius10={props.borderRadius10}
    boxShadow10={props.boxShadow10}
    overflowX10={props.overflowX10}
    overflowY10={props.overflowY10}
    width99={props.width99}
    height99={props.height99}
    marginTop99={props.marginTop99}
    marginRight99={props.marginRight99}
    marginBottom99={props.marginBottom99}
    marginLeft99={props.marginLeft99}
    paddingTop99={props.paddingTop99}
    paddingRight99={props.paddingRight99}
    paddingBottom99={props.paddingBottom99}
    paddingLeft99={props.paddingLeft99}
    display99={props.display99}
    justifyContent99={props.justifyContent99}
    alignItems99={props.alignItems99}
    gridColumns99={props.gridColumns99}
    gridGap99={props.gridGap99}
    justifyItems99={props.justifyItems99}
    justifySelf99={props.justifySelf99}
    position99={props.position99}
    top99={props.top99}
    right99={props.right99}
    bottom99={props.bottom99}
    left99={props.left99}
    minHeight99={props.minHeight99}
    minWidth99={props.minWidth99}
    zIndex99={props.zIndex99}
    backgroundColor99={props.backgroundColor99}
    backgroundImg99={props.backgroundImg99}
    backgroundSize99={props.backgroundSize99}
    backgroundPos99={props.backgroundPos99}
    backgroundRep99={props.backgroundRep99}
    maxWidth99={props.maxWidth99}
    maxHeight99={props.maxHeight99}
    objectFit99={props.objectFit99}
    borderRadius99={props.borderRadius99}
    boxShadow99={props.boxShadow99}
    overflowX99={props.overflowX99}
    overflowY99={props.overflowY99}
    textAlign92={props.textAlign92}
    width92={props.width92}
    height92={props.height92}
    marginTop92={props.marginTop92}
    marginRight92={props.marginRight92}
    marginBottom92={props.marginBottom92}
    marginLeft92={props.marginLeft92}
    paddingTop92={props.paddingTop92}
    paddingRight92={props.paddingRight92}
    paddingBottom92={props.paddingBottom92}
    paddingLeft92={props.paddingLeft92}
    display92={props.display92}
    justifyContent92={props.justifyContent92}
    alignItems92={props.alignItems92}
    flexDir92={props.flexDir92}
    gridColumns92={props.gridColumns92}
    gridGap92={props.gridGap92}
    justifyItems92={props.justifyItems92}
    justifySelf92={props.justifySelf92}
    position92={props.position92}
    top92={props.top92}
    right92={props.right92}
    bottom92={props.bottom92}
    left92={props.left92}
    minHeight92={props.minHeight92}
    minWidth92={props.minWidth92}
    zIndex92={props.zIndex92}
    backgroundColor92={props.backgroundColor92}
    backgroundImg92={props.backgroundImg92}
    backgroundSize92={props.backgroundSize92}
    backgroundPos92={props.backgroundPos92}
    backgroundRep92={props.backgroundRep92}
    maxWidth92={props.maxWidth92}
    maxHeight92={props.maxHeight92}
    objectFit92={props.objectFit92}
    borderRadius92={props.borderRadius92}
    boxShadow92={props.boxShadow92}
    overflowX92={props.overflowX92}
    overflowY92={props.overflowY92}
    display72={props.display72}
    top72={props.top72}
    maxWidth62={props.maxWidth62}
    marginLeft62={props.marginLeft62}
    marginRight62={props.marginRight62}
    width62={props.width62}
    display62={props.display62}
    position62={props.position62}
    top62={props.top62}
    right62={props.right62}
    bottom62={props.bottom62}
    left62={props.left62}
    backgroundSize62={props.backgroundSize62}
    minHeight62={props.minHeight62}
    width48={props.width48}
    flexDir48={props.flexDir48}
    marginTop48={props.marginTop48}
    marginBottom48={props.marginBottom48}
    marginLeft48={props.marginLeft48}
    marginRight48={props.marginRight48}
    alignItems48={props.alignItems48}
    display42={props.display42}
    gridColumns42={props.gridColumns42}
    gridGap42={props.gridGap42}
    marginBottom38={props.marginBottom38}
    maxWidth38={props.maxWidth38}
    onClick={props.onClick}
    aniName={props.aniName}
    aniDura={props.aniDura}
    aniIterCount={props.aniIterCount}  
    aniTimFun={props.aniTimFun} 
    keyFrames={props.keyFrames}       
    >
    {props.children}
  </DiverD>
);
const DiverD = styled.div`
  font-weight: ${props => props.fontWeight || 'normal'};
  color: ${props => props.color || '#4a5763'};
  text-align: ${props => props.textAlign || 'center'};
  width: ${props => props.width || 'auto'};
  max-width: ${props => props.maxWidth || 'none'};
  max-height: ${props => props.maxHeight || 'none'};
  height: ${props => props.height || 'auto'};
  margin-top: ${props => props.marginTop || '0'};
  margin-right: ${props => props.marginRight || '0'};
  margin-bottom: ${props => props.marginBottom || '0'};
  margin-left: ${props => props.marginLeft || '0'};
  padding-top: ${props => props.paddingTop || '0'};
  padding-right: ${props => props.paddingRight || '0'};
  padding-bottom: ${props => props.paddingBottom || '0'};
  padding-left: ${props => props.paddingLeft || '0'};
  display: ${props => props.display || 'block'};
  justify-content: ${props => props.justifyContent || 'normal'};
  align-items: ${props => props.alignItems || 'normal'};
  align-self: ${props => props.alignSelf || 'auto'};
  flex-direction: ${props => props.flexDir || 'row'};
  grid-template-columns: ${props => props.gridColumns || 'none'};
  grid-gap: ${props => props.gridGap || '0'};
  justify-items: ${props => props.justifyItems || 'legacy'};
  justify-self: ${props => props.justifySelf || 'auto'};
  position: ${props => props.position || 'static'};
  top: ${props => props.top || 'auto'};
  right: ${props => props.right || 'auto'};
  bottom: ${props => props.bottom || 'auto'};
  left: ${props => props.left || 'auto'};
  min-width: ${props => props.minWidth || 'auto'};
  min-height: ${props => props.minHeight || 'auto'};
  z-index: ${props => props.zIndex || 'auto'};
  background-color: ${props => props.backgroundColor || 'none'};
  background-image: ${props => props.backgroundImg || 'none'};
  background-size: ${props => props.backgroundSize || 'auto auto'};
  background-position: ${props => props.backgroundPos || '0% 0%'};
  background-repeat: ${props => props.backgroundRep || 'repeat'};
  object-fit: ${props => props.objectFit || 'fill'};
  border: ${props => props.border || '0'};
  border-radius: ${props => props.borderRadius || '0'};
  box-shadow: ${props => props.boxShadow || '0'};
  overflow-x: ${props => props.overflowX || 'visible'};
  overflow-y: ${props => props.overflowY || 'visible'};
  text-overflow: ${props => props.textOF || 'clip'};
  cursor: ${props => props.cursor || 'auto'};
  opacity: ${props => props.opacity || '1'};
  animation-name: ${props => props.aniName || 'none'};
  animation-duration: ${props => props.aniDura || '0s'};
  animation-iteration-count: ${props => props.aniIterCount || '1'};
  animation-timing-function: ${props => props.aniTimFun || 'ease'};
  @keyframes ${props => props.aniName} {${props => props.keyFrames}}
  @media (max-width: 1200px) {
    text-align: ${props => props.textAlign12 || props.textAlign || 'center'};
    width: ${props => props.width12 || props.width || 'auto'};
  max-width: ${props =>  props.maxWidth12 || props.maxWidth || 'none'};
  max-height: ${props => props.maxHeight12 || props.maxHeight || 'none'};
  height: ${props => props.height12 || props.height || 'auto'};
  margin-top: ${props => props.marginTop12 || props.marginTop || '0'};
  margin-right: ${props => props.marginRight12 || props.marginRight || '0'};
  margin-bottom: ${props => props.marginBottom12 || props.marginBottom || '0'};
  margin-left: ${props => props.marginLeft12 || props.marginLeft || '0'};
  padding-top: ${props => props.paddingTop12 || props.paddingTop || '0'};
  padding-right: ${props => props.paddingRight12 || props.paddingRight || '0'};
  padding-bottom: ${props => props.paddingBottom12 || props.paddingBottom || '0'};
  padding-left: ${props => props.paddingLeft12 || props.paddingLeft || '0'};
  display: ${props => props.display12 || props.display || 'block'};
  justify-content: ${props => props.width12 || props.justifyContent || 'normal'};
  align-items: ${props => props.justifyContent12 || props.alignItems || 'normal'};
  grid-template-columns: ${props => props.gridColumns12 || props.gridColumns || 'none'};
  grid-gap: ${props => props.gridGap12 || props.gridGap || '0'};
  justify-items: ${props => props.justifyItems12 || props.justifyItems || 'legacy'};
  justify-self: ${props => props.justifySelf12 || props.justifySelf || 'auto'};
  position: ${props => props.position12 || props.position || 'static'};
  top: ${props => props.top12 || props.top || 'auto'};
  right: ${props => props.right12 || props.right || 'auto'};
  bottom: ${props => props.bottom12 || props.bottom || 'auto'};
  left: ${props => props.left12 || props.left || 'auto'};
  min-width: ${props => props.minWidthh12 || props.minWidth || 'auto'};
  min-height: ${props => props.minHeight12 || props.minHeight || 'auto'};
  z-index: ${props => props.zIndex12 || props.zIndex || 'auto'};
  background-color: ${props => props.backgroundColor12 || props.backgroundColor || 'none'};
  background-image: ${props => props.backgroundImg12 || props.backgroundImg || 'none'};
  background-size: ${props => props.backgroundSize12 || props.backgroundSize || 'auto auto'};
  background-position: ${props => props.backgroundPos12 || props.backgroundPos || '0% 0%'};
  background-repeat: ${props => props.backgroundRep12 || props.backgroundRep || 'repeat'};
  object-fit: ${props => props.objectFit12 || props.objectFit || 'fill'};
  border-radius: ${props => props.borderRadius12 || props.borderRadius || '0'};
  box-shadow: ${props => props.boxShadow12 || props.boxShadow || '0'};
  overflow-x: ${props => props.overflowX12 || props.overflowX || 'visible'};
  overflow-y: ${props => props.overflowY12 || props.overflowY || 'visible'};
  }
  @media (max-width: 1000px) {
    text-align: ${props =>props.textAlign10 ||  props.textAlign12 || props.textAlign || 'center'};
    width: ${props => props.width10 ||  props.width12 || props.width || 'auto'};
  max-width: ${props => props.maxWidth10 ||    props.maxWidth12 || props.maxWidth || 'none'};
  max-height: ${props => props.maxHeight10 ||   props.maxHeight12 || props.maxHeight || 'none'};
  height: ${props => props.height10 ||   props.height12 || props.height || 'auto'};
  margin-top: ${props => props.marginTop10 ||   props.marginTop12 || props.marginTop || '0'};
  margin-right: ${props => props.marginRight10 ||   props.marginRight12 || props.marginRight || '0'};
  margin-bottom: ${props => props.marginBottom10 ||   props.marginBottom12 || props.marginBottom || '0'};
  margin-left: ${props => props.marginLeft10 ||   props.marginLeft12 || props.marginLeft || '0'};
  padding-top: ${props => props.paddingTop10 ||   props.paddingTop12 || props.paddingTop || '0'};
  padding-right: ${props => props.paddingRight10 ||   props.paddingRight12 || props.paddingRight || '0'};
  padding-bottom: ${props => props.paddingBottom10 ||   props.paddingBottom12 || props.paddingBottom || '0'};
  padding-left: ${props => props.paddingLeft10 ||   props.paddingLeft12 || props.paddingLeft || '0'};
  display: ${props => props.display10 ||   props.display12 || props.display || 'block'};
  flex-direction: ${props => props.flexDir10 ||   props.flexDir12 || props.flexDir ||  'row'};
  justify-content: ${props => props.justifyContent10 ||   props.justifyContent12 || props.justifyContent || 'normal'};
  align-items: ${props => props.alignItems10 ||   props.alignItems12 || props.alignItems || 'normal'};
  grid-template-columns: ${props => props.gridColumns10 ||   props.gridColumns12 || props.gridColumns || 'none'};
  grid-gap: ${props => props.gridGap10 ||   props.gridGap12 || props.gridGap || '0'};
  justify-items: ${props => props.justifyItems10 ||   props.justifyItems12 || props.justifyItems || 'legacy'};
  justify-self: ${props => props.justifySelf10 ||   props.justifySelf12 || props.justifySelf || 'auto'};
  position: ${props => props.position10 ||   props.position12 || props.position || 'static'};
  top: ${props => props.top10 ||   props.top12 || props.top || 'auto'};
  right: ${props => props.right10 ||   props.right12 || props.right || 'auto'};
  bottom: ${props => props.bottom10 ||   props.bottom12 || props.bottom || 'auto'};
  left: ${props => props.left10 ||   props.left12 || props.left || 'auto'};
  min-width: ${props => props.minWidth10 ||   props.minWidthh12 || props.minWidth || 'auto'};
  min-height: ${props => props.minHeight10 ||   props.minHeight12 || props.minHeight || 'auto'};
  z-index: ${props => props.zIndex10 ||   props.zIndex12 || props.zIndex || 'auto'};
  background-color: ${props => props.backgroundColor10 ||   props.backgroundColor12 || props.backgroundColor || 'none'};
  background-image: ${props => props.backgroundImg10 ||   props.backgroundImg12 || props.backgroundImg || 'none'};
  background-size: ${props => props.backgroundSize10 ||   props.backgroundSize12 || props.backgroundSize || 'auto auto'};
  background-position: ${props => props.backgroundPos10 ||   props.backgroundPos12 || props.backgroundPos || '0% 0%'};
  background-repeat: ${props => props.backgroundRep10 ||   props.backgroundRep12 || props.backgroundRep || 'repeat'};
  object-fit: ${props => props.objectFit10 ||   props.objectFit12 || props.objectFit || 'fill'};
  border-radius: ${props => props.borderRadius10 ||   props.borderRadius12 || props.borderRadius || '0'};
  box-shadow: ${props => props.boxShadow10 ||   props.boxShadow12 || props.boxShadow || '0'};
  overflow-x: ${props => props.overflowX10 ||   props.overflowX12 || props.overflowX || 'visible'};
  overflow-y: ${props => props.overflowY10 ||   props.overflowY12 || props.overflowY || 'visible'};
  }
  @media (max-width: 992px) {
    text-align: ${props =>props.textAlign99 || props.textAlign10 ||  props.textAlign12 || props.textAlign || 'center'};
    width: ${props =>props.width99 ||  props.width10 ||  props.width12 || props.width || 'auto'};
  max-width: ${props => props.maxWidth99 ||  props.maxWidth10 ||    props.maxWidth12 || props.maxWidth || 'none'};
  max-height: ${props =>props.maxHeight99 ||  props.maxHeight10 ||   props.maxHeight12 || props.maxHeight || 'none'};
  height: ${props =>props.height99 ||  props.height10 ||   props.height12 || props.height || 'auto'};
  margin-top: ${props =>props.marginTop99 ||  props.marginTop10 ||   props.marginTop12 || props.marginTop || '0'};
  margin-right: ${props =>props.marginRight99 ||  props.marginRight10 ||   props.marginRight12 || props.marginRight || '0'};
  margin-bottom: ${props =>props.marginBottom99 ||  props.marginBottom10 ||   props.marginBottom12 || props.marginBottom || '0'};
  margin-left: ${props =>props.marginLeft99 ||  props.marginLeft10 ||   props.marginLeft12 || props.marginLeft || '0'};
  padding-top: ${props =>props.paddingTop99 ||  props.paddingTop10 ||   props.paddingTop12 || props.paddingTop || '0'};
  padding-right: ${props =>props.paddingRight99 ||  props.paddingRight10 ||   props.paddingRight12 || props.paddingRight || '0'};
  padding-bottom: ${props =>props.paddingBottom99 ||  props.paddingBottom10 ||   props.paddingBottom12 || props.paddingBottom || '0'};
  padding-left: ${props =>props.paddingLeft10 ||  props.paddingLeft10 ||   props.paddingLeft12 || props.paddingLeft || '0'};
  display: ${props =>props.display99 ||  props.display10 ||   props.display12 || props.display || 'block'};
  justify-content: ${props =>props.justifyContent99 ||  props.justifyContent10 ||   props.justifyContent12 || props.justifyContent || 'normal'};
  align-items: ${props =>props.alignItems99 ||  props.alignItems10 ||   props.alignItems12 || props.alignItems || 'normal'};
  grid-template-columns: ${props =>props.gridColumns99 ||  props.gridColumns10 ||   props.gridColumns12 || props.gridColumns || 'none'};
  grid-gap: ${props =>props.gridGap99 ||  props.gridGap10 ||   props.gridGap12 || props.gridGap || '0'};
  justify-items: ${props =>props.justifyItems99 ||  props.justifyItems10 ||   props.justifyItems12 || props.justifyItems || 'legacy'};
  justify-self: ${props =>props.justifySelf99 ||  props.justifySelf10 ||   props.justifySelf12 || props.justifySelf || 'auto'};
  position: ${props =>props.position99 ||  props.position10 ||   props.position12 || props.position || 'static'};
  top: ${props =>props.top99 ||  props.top10 ||   props.top12 || props.top || 'auto'};
  right: ${props =>props.right99 ||  props.right10 ||   props.right12 || props.right || 'auto'};
  bottom: ${props =>props.bottom99 ||  props.bottom10 ||   props.bottom12 || props.bottom || 'auto'};
  left: ${props =>props.left99 ||  props.left10 ||   props.left12 || props.left || 'auto'};
  min-width: ${props =>props.minWidth99 ||  props.minWidth10 ||   props.minWidthh12 || props.minWidth || 'auto'};
  min-height: ${props =>props.minHeight99 ||  props.minHeight10 ||   props.minHeight12 || props.minHeight || 'auto'};
  z-index: ${props =>props.zIndex99 ||  props.zIndex10 ||   props.zIndex12 || props.zIndex || 'auto'};
  background-color: ${props =>props.backgroundColor99 ||  props.backgroundColor10 ||   props.backgroundColor12 || props.backgroundColor || 'none'};
  background-image: ${props =>props.backgroundImg99 ||  props.backgroundImg10 ||   props.backgroundImg12 || props.backgroundImg || 'none'};
  background-size: ${props =>props.backgroundSize99 ||  props.backgroundSize10 ||   props.backgroundSize12 || props.backgroundSize || 'auto auto'};
  background-position: ${props =>props.backgroundPos99 ||  props.backgroundPos10 ||   props.backgroundPos12 || props.backgroundPos || '0% 0%'};
  background-repeat: ${props =>props.backgroundRep99 ||  props.backgroundRep10 ||   props.backgroundRep12 || props.backgroundRep || 'repeat'};
  object-fit: ${props =>props.objectFit99 ||  props.objectFit10 ||   props.objectFit12 || props.objectFit || 'fill'};
  border-radius: ${props =>props.borderRadius99 ||  props.borderRadius10 ||   props.borderRadius12 || props.borderRadius || '0'};
  box-shadow: ${props =>props.boxShadow99 ||  props.boxShadow10 ||   props.boxShadow12 || props.boxShadow || '0'};
  overflow-x: ${props =>props.overflowX99 ||  props.overflowX10 ||   props.overflowX12 || props.overflowX || 'visible'};
  overflow-y: ${props =>props.overflowY99 ||  props.overflowY10 ||   props.overflowY12 || props.overflowY || 'visible'};
  }
  @media (max-width: 920px) {
    text-align: ${props =>props.textAlign92 || props.textAlign99 || props.textAlign10 ||  props.textAlign12 || props.textAlign || 'center'};
    width: ${props =>props.width92 || props.width99 ||  props.width10 ||  props.width12 || props.width || 'auto'};
  max-width: ${props => props.maxWidth92 || props.maxWidth99 ||  props.maxWidth10 ||    props.maxWidth12 || props.maxWidth || 'none'};
  max-height: ${props =>props.maxHeight92 || props.maxHeight99 ||  props.maxHeight10 ||   props.maxHeight12 || props.maxHeight || 'none'};
  height: ${props =>props.height92 ||props.height99 ||  props.height10 ||   props.height12 || props.height || 'auto'};
  margin-top: ${props =>props.marginTop92 || props.marginTop99 ||  props.marginTop10 ||   props.marginTop12 || props.marginTop || '0'};
  margin-right: ${props =>props.marginRight92 || props.marginRight99 ||  props.marginRight10 ||   props.marginRight12 || props.marginRight || '0'};
  margin-bottom: ${props =>props.marginBottom92 || props.marginBottom99 ||  props.marginBottom10 ||   props.marginBottom12 || props.marginBottom || '0'};
  margin-left: ${props => props.marginLeft92 || props.marginLeft99 ||  props.marginLeft10 ||   props.marginLeft12 || props.marginLeft || '0'};
  padding-top: ${props =>props.paddingTop92 ||props.paddingTop99 ||  props.paddingTop10 ||   props.paddingTop12 || props.paddingTop || '0'};
  padding-right: ${props =>props.paddingRight92 || props.paddingRight99 ||  props.paddingRight10 ||   props.paddingRight12 || props.paddingRight || '0'};
  padding-bottom: ${props =>props.paddingBottom92 || props.paddingBottom99 ||  props.paddingBottom10 ||   props.paddingBottom12 || props.paddingBottom || '0'};
  padding-left: ${props =>props.paddingLeft92 || props.paddingLeft99 ||  props.paddingLeft10 ||   props.paddingLeft12 || props.paddingLeft || '0'};
  display: ${props =>props.display92 || props.display99 ||  props.display10 ||   props.display12 || props.display || 'block'};
  justify-content: ${props =>props.justifyContent92 || props.justifyContent99 ||  props.justifyContent10 ||   props.justifyContent12 || props.justifyContent || 'normal'};
  align-items: ${props =>props.alignItems92 || props.alignItems99 ||  props.alignItems10 ||   props.alignItems12 || props.alignItems || 'normal'};
  flex-direction: ${props => props.flexDir92 || props.flexDir99 ||  props.flexDir10 ||   props.flexDir12 || props.flexDir ||  'row'};
  grid-template-columns: ${props =>props.gridColumns92 || props.gridColumns99 ||  props.gridColumns10 ||   props.gridColumns12 || props.gridColumns || 'none'};
  grid-gap: ${props =>props.gridGap92 || props.gridGap99 ||  props.gridGap10 ||   props.gridGap12 || props.gridGap || '0'};
  justify-items: ${props =>props.justifyItems92 || props.justifyItems99 ||  props.justifyItems10 ||   props.justifyItems12 || props.justifyItems || 'legacy'};
  justify-self: ${props =>props.justifySelf92 || props.justifySelf99 ||  props.justifySelf10 ||   props.justifySelf12 || props.justifySelf || 'auto'};
  position: ${props =>props.position92 || props.position99 ||  props.position10 ||   props.position12 || props.position || 'static'};
  top: ${props =>props.top92 || props.top99 ||  props.top10 ||   props.top12 || props.top || 'auto'};
  right: ${props =>props.right92 || props.right99 ||  props.right10 ||   props.right12 || props.right || 'auto'};
  bottom: ${props =>props.bottom92 || props.bottom99 ||  props.bottom10 ||   props.bottom12 || props.bottom || 'auto'};
  left: ${props =>props.left92 || props.left99 ||  props.left10 ||   props.left12 || props.left || 'auto'};
  min-width: ${props =>props.minWidth92 || props.minWidth99 ||  props.minWidth10 ||   props.minWidthh12 || props.minWidth || 'auto'};
  min-height: ${props =>props.minHeight92 || props.minHeight99 ||  props.minHeight10 ||   props.minHeight12 || props.minHeight || 'auto'};
  z-index: ${props =>props.zIndex92 || props.zIndex99 ||  props.zIndex10 ||   props.zIndex12 || props.zIndex || 'auto'};
  background-color: ${props =>props.backgroundColor92 || props.backgroundColor99 ||  props.backgroundColor10 ||   props.backgroundColor12 || props.backgroundColor || 'none'};
  background-image: ${props =>props.backgroundImg92 || props.backgroundImg99 ||  props.backgroundImg10 ||   props.backgroundImg12 || props.backgroundImg || 'none'};
  background-size: ${props =>props.backgroundSize92 || props.backgroundSize99 ||  props.backgroundSize10 ||   props.backgroundSize12 || props.backgroundSize || 'auto auto'};
  background-position: ${props =>props.backgroundPos92 || props.backgroundPos99 ||  props.backgroundPos10 ||   props.backgroundPos12 || props.backgroundPos || '0% 0%'};
  background-repeat: ${props =>props.backgroundRep99 || props.backgroundRep99 ||  props.backgroundRep10 ||   props.backgroundRep12 || props.backgroundRep || 'repeat'};
  object-fit: ${props =>props.objectFit92 ||  props.objectFit10 ||   props.objectFit12 || props.objectFit || 'fill'};
  border-radius: ${props =>props.borderRadius92 || props.borderRadius99 ||  props.borderRadius10 ||   props.borderRadius12 || props.borderRadius || '0'};
  box-shadow: ${props =>props.boxShadow92 || props.boxShadow99 ||  props.boxShadow10 ||   props.boxShadow12 || props.boxShadow || '0'};
  overflow-x: ${props =>props.overflowX92 || props.overflowX99 ||  props.overflowX10 ||   props.overflowX12 || props.overflowX || 'visible'};
  overflow-y: ${props =>props.overflowY92 || props.overflowY99 ||  props.overflowY10 ||   props.overflowY12 || props.overflowY || 'visible'};
}
  @media (max-width: 720px) {
    display: ${props => props.display72 || props.display76 || props.display92 || props.display99 ||  props.display10 ||  props.display12 || props.display || 'auto'};
    top: ${props => props.top72 || props.top76 || props.top92 || props.top99 ||  props.top10 ||  props.top12 || props.top || 'auto'};
  }
  @media (max-width: 620px) {
    max-width: ${props => props.maxWidth62 || props.maxWidth72 || props.maxWidth76 || props.maxWidth92 || props.maxWidth99 ||  props.maxWidth10 ||   props.maxWidth12 || props.maxWidth || 'none'};
    margin-right: ${props => props.marginRight62 || props.marginRight72 || props.marginRight76 || props.marginRight92 || props.marginRight99 ||  props.marginRight10 ||   props.marginRight12 || props.marginRight || '0'};
    margin-left: ${props => props.marginLeft62 || props.marginLeft72 || props.marginLeft76 || props.marginLeft92 || props.marginLeft99 ||  props.marginLeft10 ||   props.marginLeft12 || props.marginLeft || '0'};
    width: ${props => props.width62 || props.width72 || props.width76 || props.width92 || props.width99 ||  props.width10 ||  props.width12 || props.width || 'auto'};
    display: ${props => props.display62 || props.display72 || props.display76 || props.display92 || props.display99 ||  props.display10 ||  props.display12 || props.display || 'auto'};
    position: ${props => props.position62 || props.position72 || props.position76 || props.position92 || props.position99 ||  props.position10 ||   props.position12 || props.position || 'static'};
    top: ${props => props.top62 || props.top72 || props.top76 || props.top92 || props.top99 ||  props.top10 ||  props.top12 || props.top || 'auto'};
    right: ${props => props.right62 || props.right72 || props.right76 || props.right92 || props.right99 ||  props.right10 ||  props.right12 || props.right || 'auto'};
    bottom:${props => props.bottom62 || props.bottom72 || props.bottom76 || props.bottom92 || props.bottom99 ||  props.bottom10 ||  props.bottom12 || props.bottom || 'auto'};
    left: ${props => props.left62 || props.left72 || props.left76 || props.left92 || props.left99 ||  props.left10 ||  props.left12 || props.left || 'auto'};
    background-size: ${props => props.backgroundSize62 || props.backgroundSize72 || props.backgroundSize76 || props.backgroundSize92 || props.backgroundSize99 ||  props.backgroundSize10 ||  props.backgroundSize12 || props.backgroundSize || 'auto auto'};
    min-height: ${props => props.minHeight62 || props.minHeight72 || props.minHeight76 || props.minHeight92 || props.minHeight99 ||  props.minHeight10 ||  props.minHeight12 || props.minHeight || 'auto auto'};
  }
  @media (max-width: 480px) {
    width: ${props =>props.width48 || props.width62 || props.width72 || props.width76 || props.width92 || props.width99 ||  props.width10 ||  props.width12 || props.width || 'auto'};
    flex-direction: ${props => props.flexDir48  || props.flexDir62 || props.flexDir72 || props.flexDir76 ||props.flexDir92 || props.flexDir99 ||  props.flexDir10 ||   props.flexDir12 || props.flexDir ||  'row'};
    margin-top: ${props =>props.marginTop48 || props.marginTop62 || props.marginTop72 || props.marginTop76 || props.marginTop92 || props.marginTop99 ||  props.marginTop10 ||   props.marginTop12 || props.marginTop || '0'};
    margin-bottom: ${props =>props.marginBottom48 || props.marginBottom62 || props.marginBottom72 || props.marginBottom76 || props.marginBottom92 || props.marginBottom99 ||  props.marginBottom10 ||   props.marginBottom12 || props.marginBottom || '0'};
    margin-left: ${props =>props.marginLeft48 || props.marginLeft62 || props.marginLeft72 || props.marginLeft76 || props.marginLeft92 || props.marginLeft99 ||  props.marginLeft10 ||   props.marginLeft12 || props.marginLeft || '0'};
    margin-right: ${props =>props.marginRight48 || props.marginRight62 || props.marginRight72 || props.marginRight76 || props.marginRight92 || props.marginRight99 ||  props.marginRight10 ||   props.marginRight12 || props.marginRight || '0'};
    align-items: ${props =>props.alignItems48 || props.alignItems62 || props.alignItems72 || props.alignItems76 || props.alignItems92 || props.alignItems99 ||  props.alignItems10 ||   props.alignItems12 || props.alignItems || 'normal'};
  }
  @media (max-width: 420px) {
    display: ${props =>props.display42 || props.display48 || props.display62 || props.display72 || props.display76 || props.display92 || props.display99 ||  props.display10 ||  props.display12 || props.display || 'auto'};
    grid-template-columns: ${props =>props.gridColumns42 || props.gridColumns48 || props.gridColumns62 || props.gridColumns72 || props.gridColumns76 || props.gridColumns92 || props.gridColumns99 ||  props.gridColumns10 ||   props.gridColumns12 || props.gridColumns || 'none'};
    grid-gap: ${props =>props.gridGap42 ||props.gridGap48 ||props.gridGap62 ||props.gridGap72 || props.gridGap76 || props.gridGap92 || props.gridGap99 ||  props.gridGap10 ||   props.gridGap12 || props.gridGap || '0'};
  }
  @media (max-width: 380px) {
    max-width: ${props =>props.maxWidth38 || props.maxWidth42 ||props.maxWidth48 ||props.maxWidth62 ||props.maxWidth72 || props.maxWidth76 || props.maxWidth92 || props.maxWidth99 ||  props.maxWidth10 ||   props.maxWidth12 || props.maxWidth || 'none'};
    margin-bottom: ${props =>props.marginBottom38 || props.marginBottom42 ||props.marginBottom48 ||props.marginBottom62 ||props.marginBottom72 || props.marginBottom76 || props.marginBottom92 || props.marginBottom99 ||  props.marginBottom10 ||   props.marginBottom12 || props.marginBottom || '0'};
  }

`;

export default Diver;

/*
  @media (max-width: 768px) {
    background-color: ${props => props.backgroundColorh || props.backgroundColor || 'none'};

  }
  @media (max-width: 620px) {

  }
  @media (max-width: 480px) {

  }
  @media (max-width: 380px) {

  }
  @media (max-width: 320px) {

  }




    &:hover {
    background-color: ${props => props.backgroundColorh || props.backgroundColor42 || props.backgroundColor48 || props.backgroundColor62 || props.backgroundColor72 || props.backgroundColor76 || props.backgroundColor92 || props.backgroundColor99 ||  props.backgroundColor10 ||  props.backgroundColor12 || props.backgroundColor || 'none'};
    border: ${props => props.borderh || props.border42 || props.border48 || props.border62 || props.border72 || props.border76 || props.border92 || props.border99 ||  props.border10 ||  props.border12 || props.border || '0'};
    border-radius: ${props => props.borderRadiush || props.borderRadius42 || props.borderRadius48 || props.borderRadius62 || props.borderRadius72 || props.borderRadius76 || props.borderRadius92 || props.borderRadius99 ||  props.borderRadius10 ||  props.borderRadius12 || props.borderRadius || '0'};
    padding-top: ${props => props.paddingToph || props.paddingTop42 || props.paddingTop48 || props.paddingTop62 || props.paddingTop72 || props.paddingTop76 || props.paddingTop92 || props.paddingTop99 ||  props.paddingTop10 ||  props.paddingTop12 || props.paddingTop || '0'};
    padding-right: ${props => props.paddingRighth || props.paddingRight42 || props.paddingRight48 || props.paddingRight62 || props.paddingRight72 || props.paddingRight76 || props.paddingRight92 || props.paddingRight99 ||  props.paddingRight10 ||  props.paddingRight12 || props.paddingRight || '0'};
    padding-bottom: ${props => props.paddingBottomh || props.paddingBottom42 || props.paddingBottom48 || props.paddingBottom62 || props.paddingBottom72 || props.paddingBottom76 || props.paddingBottom92 || props.paddingBottom99 ||  props.paddingBottom10 ||  props.paddingBottom12 || props.paddingBottom || '0'};
    padding-left: ${props => props.paddingLefth || props.paddingLeft42 || props.paddingLeft48 || props.paddingLeft62 || props.paddingLeft72 || props.paddingLeft76 || props.paddingLeft92 || props.paddingLeft99 ||  props.paddingLeft10 ||  props.paddingLeft12 || props.paddingLeft || '0'};

  }


*/