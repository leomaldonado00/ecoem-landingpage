import React from 'react';
import styled from 'styled-components';
// import { Link } from "react-router-dom";

const ImgGral = props => {
  return (
      <Img
        src={props.src}
        alt={props.alt}
        width={props.width}
        height={props.height}
        width92={props.width92}
        height92={props.height92}
        width62={props.width62}
        height62={props.height62}
        cursor={props.cursor}
      />
  );
};

const Img = styled.img`
width: ${props => props.width || "auto" };
height: ${props => props.height || "auto" };
cursor: ${props => props.cursor || "auto" };
@media (max-width: 920px) {
  width: ${props => props.width92 || props.width99 ||  props.width10 ||  props.width12 || props.width || 'auto'};
  height: ${props => props.height92 || props.height99 ||  props.height10 ||  props.height12 || props.height || 'auto'};
}
@media (max-width: 620px) {
    width: ${props => props.width62 || props.width72 || props.width76 || props.width92 || props.width99 ||  props.width10 ||  props.width12 || props.width || 'auto'};
    height: ${props => props.height62 || props.height72 || props.height76 || props.height92 || props.height99 ||  props.height10 ||  props.height12 || props.height || 'auto'};
}

`;

export default ImgGral;
