import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Diver from './Diver';
// import Text from './Text';

const CardServices = props => (
        <CardServicesDiv flexDir62={props.flexDir62} >
          <ImgServices src={props.img} width={props.widthImg} height={props.heightImg} width62={props.widthImg62} height62={props.heightImg62} alt="service" />
          <Texto alignItems62={props.alignItems62}>
            <Tittle color="#0046B9" textAlign62={props.textAlign62}>{props.title}</Tittle>
            <Tittle color="#262626" textAlign62={props.textAlign62}>{props.subtitleT}</Tittle>
            <Tittle color="#262626" textAlign62={props.textAlign62}>{props.subtitleB}</Tittle>
            <Link to="/services" style={{ textDecoration: 'none' }}>
              <Diver display="flex" marginTop="15px" alignItems="flex-end" cursor="pointer">
                  <Description>{props.description}</Description>
                  <Diver marginLeft="15px" cursor="pointer" height="15px" width="15px" display="flex" justifyContent="center" alignItems="center">
                      <ImgNext src="/assets/images/nextlight.png" alt="next" />
                  </Diver>
              </Diver>
            </Link>
          </Texto>
        </CardServicesDiv>
    );

    const CardServicesDiv = styled.div`
    width: 100%;
    max-width: 300px;
    height: auto;
    display: grid;
    grid-gap: 3px;
    justify-items: center;
    grid-template-rows: min-content min-content;
    /* margin-top: 100px; */

    @media (max-width: 620px) {
      display: flex;
    flex-direction: ${props => props.flexDir62 || "row" };
    justify-content: space-around;
    max-width: none;
    width: 95%;
  }
  @media (max-width: 480px) {
    justify-content: space-between;
}

  
  `;
  const ImgServices = styled.img`
    width: ${props => props.width || "200px" };
    height: ${props => props.height || "auto" };
    @media (max-width: 620px) {
      width: ${props => props.width62 || "200px" };
      height: ${props => props.height62 || "auto" };
    }  
  `;
  const ImgNext = styled.img`
  width: 15px;
  height: 15px;
  `;

  const Tittle = styled.h3`
    font-size: 20px;
    color: ${props => props.color || "#1a1a1a" };
    margin: 0px 0px 0px 0px;
    text-align: left;

    @media (max-width: 620px) {
      text-align: ${props => props.textAlign62 || "center" };
    }
  
  `;
  const Description = styled.p`
    font-size: 16px;
    color: #999999;
    margin: 0;
    font-weight: 300;
    border-radius: 3px;
    text-align: center;

    /* cursor:pointer; */
  `;
  const Texto = styled.div`
   width:100%;
   height:auto;
   display: grid;
      justify-content: center;
      padding: 15px 0px;
   /*background:${props => (props.hover ? '#fafafa' : 'transparent')};*/
   /*border-radius:12px;*/

   @media (max-width: 620px) {
    width: auto;
    display: flex;
  flex-direction: column;
  align-items: ${props => props.alignItems62 || "center" };

}

  
  `;
  export default CardServices;
