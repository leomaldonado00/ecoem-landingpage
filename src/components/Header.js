import React, { useState} from 'react';
import { connect, useDispatch } from 'react-redux';
import styled from 'styled-components';
import { MdMenu } from 'react-icons/md';
import { FaTwitterSquare, FaFacebookF, FaInstagram } from 'react-icons/fa';
import { setEs, setEn } from '../ducks/i18n';
import langStrings from '../i18n';
import Text from './Text';
import Diver from './Diver';
import Sidebar from './Sidebar';

const Header = props => {
  const dispatch = useDispatch();
  function langInit(props) {
    if (props.langCode === 'en') {
      return false;
    }
    return true;
  }

  const [menu, setMenu] = useState(false);
  const [lang] = useState(langInit(props)); // true es false en
  const [langColor, setLangColor] = useState(langInit(props)); // true es false en
  const [item, setItem] = useState('');
  const itemHeader = props.itemHeader;

  /*
  useEffect(() => {
  }, [props.langCode]);
  */
  function changeRoute(route) {

    if (props.history.location.pathname === '/') {
      const element = document.getElementById(route);
      const offset = 1;
      const bodyRect = document.body.getBoundingClientRect().top;
      const elementRect = element.getBoundingClientRect().top;
      const elementPosition = elementRect - bodyRect;
      const offsetPosition = elementPosition - offset;
      window.scrollTo({
        top: offsetPosition,
        behavior: 'smooth'
      });  
    } else if (props.history.location.pathname !== '/') {
      props.history.push('/', {
        ancla: route,
      });
    } else {
      props.history.push('/');
    } 

  }
  
  return (
    <div>
      <HeaderDiv history={props.history}>
        <WrapArrowLogo>
  {/* props.arrow ? <ArrowBack history={props.history} /> : null */}
          <LogoEnterp
            src="/assets/images/ecoemlogo.png"
            alt="Ecoem"
            className="opacity"
            onClick={() => changeRoute('intro')} 
        /> 
        </WrapArrowLogo>
        <Links>
          <HomeDiv>
            <LinkHome
              onClick={() => {
                setItem('intro');
                changeRoute('intro');
              }}
              item={item || itemHeader}
            >
              {"Home"}
            </LinkHome>
          </HomeDiv>
          <ServicesDiv lang={lang.toString()}>
            <LinkServices
              onClick={() => {
                props.history.push('/services');
              }}
              item={itemHeader === "services" ? "services" : null}
            >
              {langStrings.services}
            </LinkServices>
          </ServicesDiv>
          <PortfolioDiv lang={lang.toString()}>
            <LinkPortfolio
              onClick={() => {
                setItem('portfolio');
                changeRoute('portfolio');
              }}
              item={item || itemHeader}
            >
              {langStrings.portfolio}
            </LinkPortfolio>
          </PortfolioDiv>
          <ContactDiv>
            <LinkContact
              onClick={() => {
                setItem('contact');
                changeRoute('contact');
              }}
              item={item || itemHeader}
            >
              {langStrings.contact}
            </LinkContact>
          </ContactDiv>

          <BlogDiv>
            <LinkBlog
              onClick={() => {
                setItem('footer');
                changeRoute('footer');
              }}
              item={item || itemHeader}
            >
              {"Blog"}
            </LinkBlog>
          </BlogDiv>
          <a href="https://www.facebook.com/ecoem.ve" target="blank">
            <Diver marginRight99="8px" cursor="pointer"> 
              <FaFacebookF size={20} color="#FFF" />
            </Diver>
          </a>
          <a href="https://www.instagram.com/ecoem.studio/" target="blank">
            <Diver marginRight99="8px" cursor="pointer">
              <FaInstagram size={20} color="#FFF" />
            </Diver>
          </a>
          <a href="https://twitter.com/ecoem_ve" target="blank">
            <Diver marginRight99="8px" cursor="pointer">
              <FaTwitterSquare size={20} color="#FFF" />
            </Diver>
          </a>
            <Diver display="flex" marginLeft99="15px">
                <Text color={langColor ? "#0046B9" : "silver"} marginRight="2px" fontSize="16px" cursor="pointer" onClick={() => {setLangColor(true); dispatch(setEs());}}>ES</Text>
                <Text color={langColor ? "silver" : "#0046B9"} fontSize="16px" cursor="pointer" onClick={() => {setLangColor(false); dispatch(setEn());}}>EN</Text>
            </Diver>

        </Links>
       
      </HeaderDiv>



        <HeaderDivMov>

            <Toggle className="opacity">
              <MdMenu color="#0046B9" size={38} 
              onClick={() => setMenu(!menu)} 
              /> 
            </Toggle>

            <WrapArrowLogo>

          {/* props.arrow ? <ArrowBack history={props.history} /> : null */}
          <LogoEnterp
            src="/assets/images/ecoemlogo.png"
            alt="Ecoem"
            className="opacity"
            onClick={() => {
              changeRoute('intro');
              setMenu(false);
            }} 
          /> 
          </WrapArrowLogo>

          <Diver display="flex" marginRight="80px" marginRight62="10px">
                <Text color={langColor ? "#0046B9" : "silver"} marginRight="2px" fontSize="16px" fontSize99="20px" cursor="pointer" onClick={() => {setLangColor(true); dispatch(setEs());}}>ES</Text>
                <Text color={langColor ? "silver" : "#0046B9"} fontSize="16px" fontSize99="20px" cursor="pointer" onClick={() => {setLangColor(false); dispatch(setEn());}}>EN</Text>
          </Diver>

            <Sidebar showMenuTemp={menu} itemHeader={itemHeader} history={props.history} changeRoute={(route)=>{ changeRoute(route); setTimeout(() => {setMenu(false);}, 300);}} />

        </HeaderDivMov>

    </div>
  );
};

const WrapArrowLogo = styled.div`
  width: 100%;
  max-width: 150px;
  margin-left: 80px;
  display: flex;
  align-items: center;
  justify-content: space-between;

  @media (max-width: 620px) {
    margin-left: 5px;
    max-width:none;
    width:auto;
  }
`;
const HeaderDiv = styled.div`
  position: fixed;
  top: 0px;
  z-index: 999999;
  width: 100%;
  height: 70px;
  background: #262626;
  display: flex;
  justify-content: space-between;
  height: 64px;
  align-items: center;
  padding-top: 20px;

  @media (max-width: 992px) {
    display:none;
  }

`;
const HeaderDivMov = styled.div`
display:none;
  position: fixed;
  top: 0px;
  z-index: 999999;
  width: 100%;
  height: 70px;
  background: #262626;
  justify-content: space-between;
  height: 64px;
  align-items: center;
  padding-top: 20px;

  @media (max-width: 992px) {
    display:flex;
  }

`;
const LogoEnterp = styled.img`
  width: 113px;
  height: auto;

  @media (max-width: 620px) {
    /* margin-left: 5px; */
    width:auto;
    height:22px;
  }
`;
const Links = styled.div`
  height: auto;
  /*background: red;*/
  width: 100%;
  max-width: 705px;
  margin-right: 80px;
  display: flex;
  height: max-content;
  justify-content: space-evenly;
  align-items: center;

  @media (max-width: 992px) {
    justify-content: flex-end;
  }
  @media (max-width: 620px) {
    margin-right: 5px;
  }
`;
const LinkHome = styled.p`
  text-align: center;
  height: auto;
  /*background: transparent;*/
  width: auto;
  border-radius: 40px;
  padding: 3px 8px;

  color: ${props => (props.item === 'intro' ? '#0046B9' : '#fff')};
  font-weight: ${props => (props.item === 'intro' ? '600' : '300')};

  cursor: pointer;
  &:hover {
    /*background-color: green;*/
    color: #0046B9;
  }

  @media (max-width: 992px) {
    display: none;
  }
`;
const LinkContact = styled.p`
  text-align: center;
  height: auto;
  /*background: transparent;*/
  width: auto;
  border-radius: 40px;
  padding: 3px 8px;
  color: ${props => (props.item === 'contact' ? '#0046B9' : '#fff')};
  font-weight: ${props => (props.item === 'contact' ? '600' : '300')};
  /* transition-duration: 0.6s;
  transition-timing-function: ease-out; */
  cursor: pointer;
  &:hover {
    /*background-color: green;*/
    color: #0046B9;
  }

  @media (max-width: 992px) {
    display: none;
  }
`;
const LinkBlog = styled.p`
  text-align: center;
  height: auto;
  /*background: transparent;*/
  width: auto;
  border-radius: 40px;
  padding: 3px 8px;
  color: ${props => (props.item === 'footer' ? '#0046B9' : '#fff')};
  font-weight: ${props => (props.item === 'footer' ? '600' : '300')};
  /* transition-duration: 0.6s;
  transition-timing-function: ease-out; */
  cursor: pointer;
  &:hover {
    /*background-color: green;*/
    color: #0046B9;
  }

  @media (max-width: 992px) {
    display: none;
  }
`;

const LinkPortfolio = styled.p`
  text-align: center;
  height: auto;
  /*background: transparent;*/
  width: auto;
  border-radius: 40px;
  padding: 3px 8px;
  color: ${props => (props.item === 'portfolio' ? '#0046B9' : '#fff')};
  font-weight: ${props => (props.item === 'portfolio' ? '600' : '300')};

  cursor: pointer;
  &:hover {
    /*background-color: green;*/
    color: #0046B9;
  }

  @media (max-width: 992px) {
    display: none;
  }
`;
const LinkServices = styled.p`
  text-align: center;
  height: auto;
  /*background: transparent;*/
  width: auto;
  border-radius: 40px;
  padding: 3px 8px;
  color: ${props => (props.item === 'services' ? '#0046B9' : '#fff')};
  font-weight: ${props => (props.item === 'services' ? '600' : '300')};

  cursor: pointer;
  &:hover {
    /*background-color: green;*/
    color: #0046B9;
  }

  @media (max-width: 992px) {
    display: none;
  }
`;

const Toggle = styled.div`
  width: auto;
  height: 40px;
  display: none;

  @media (max-width: 992px) {
    display: flex;
    align-items: center;
    margin-left: 80px;
  }
  @media (max-width: 620px) {
    margin-left: 10px;
  }
`;
const HomeDiv = styled.div`
  width: 60px;
  height: auto;

  @media (max-width: 992px) {
    display: none;
  }
`;
const ContactDiv = styled.div`
  width: 100px;
  height: auto;

  @media (max-width: 992px) {
    display: none;
  }
`;
const BlogDiv = styled.div`
  width: 50px;
  height: auto;

  @media (max-width: 992px) {
    display: none;
  }
`;
const ServicesDiv = styled.div`
  /* width: 134px; */
  width: ${props => (props.lang === 'true' ? '75px' : '85px' || '85px')};

  height: auto;

  @media (max-width: 992px) {
    display: none;
  }
`;
const PortfolioDiv = styled.div`
  /* width: 154px; */
  width: ${props => (props.lang === 'true' ? '95px' : '85px' || '85px')};

  height: auto;

  @media (max-width: 992px) {
    display: none;
  }
`;

function mapStateToProps(state) {
  return {
    langCode: state.i18nStore.langCode,
  };
}

export default connect(
  mapStateToProps,
  /* {
    setEs,
    setEn,
  }, */
)(Header); 

