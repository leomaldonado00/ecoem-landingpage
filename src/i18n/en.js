const en = {
  // header
  services: 'Services',
  contact: 'Contact',
  portfolio: 'Portfolio',
  // introLanding
  we_are: 'We are a creative studio -',
  focused_on: 'focused on',
  investment: 'investment',
  cocreation: 'co creation',
  in_tecnology: 'in technology-based projects',
  and_systemic: 'and systemic impact',
  // Experience
  experience: 'Experience',
  // Portfolio
  // history
  history: 'History',
  networks: 'Networks and Allies',
  // form
  errorRequired: 'the field is required',
  validEmail: 'Make sure a correct email format',
  validCountry: 'Make sure a correct country format',
};

export default en;
