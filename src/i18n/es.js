const es = {
  // Header
  services: 'Servicios',
  contact: 'Contacto',
  portfolio: 'Portafolio',
  // IntroLanding
  we_are: 'Somos un estudio creativo -',
  focused_on: 'enfocado en la',
  investment: 'inversión',
  cocreation: 'co creación',
  in_tecnology: 'de proyectos con base tecnológica',
  and_systemic: 'e impacto sistémico',
  // Experience
  experience: 'Experiencia',
  // Portfolio
  // history
  history: 'Historia',
  networks: 'Redes & Aliados',
  // form
  errorRequired: 'El campo es requerido',
  validEmail: 'Asegúrese de un formato de email correcto',
  validCountry: 'Asegúrese de un formato de país correcto',
};

export default es;
