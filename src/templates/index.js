import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Landing from './landing/Landing';
import ServicesDetail from './servicesDetail/ServicesDetail';

const Routes = () => (
  <Router>
        <Route exact path="/" component={Landing} />
        <Route exact path="/services" component={ServicesDetail} />
  </Router>
);

export default Routes;
