import React from 'react';
import Diver from '../../components/Diver';
import Text from '../../components/Text';

const Forward = props => (
    <Diver
        display="flex"
        flexDir="column"
        alignItems="center"
        width="100%"
        height="auto"
        backgroundColor="#0046B9"
        marginTop="100px"
    >
            <Diver maxWidth="1200px" width="90%" width62="95%" marginTop="25px" marginBottom="25px">
                <Text fontSize="184px" fontSize92="140px" fontSize62="80px" fontWeight="900" lineHeight="0.9" textAlign="center" color="#262626">Forward</Text>
                <Text fontSize="184px" fontSize92="140px" fontSize62="80px" fontWeight="900" lineHeight="0.9" textAlign="center" color="#262626">Forward</Text>
                <Text fontSize="184px" fontSize92="140px" fontSize62="80px" fontWeight="900" lineHeight="0.9" textAlign="center" color="#262626">Forward</Text>
            </Diver>
    </Diver>

);

export default Forward;
