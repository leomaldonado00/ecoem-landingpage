import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { langCode } from '../../ducks/i18n';
// import langStrings from '../../i18n';
import ImgGral from '../../components/ImgGral';
import Diver from '../../components/Diver';
import Text from '../../components/Text';
import Button from '../../components/Button';

const ServiceSection = (props) => {

  const langCodeT = useSelector(state => langCode(state));
  useEffect(() => {
  }, [langCodeT]);

    return (
        <Diver
        display="flex"
        flexDir="column"
        alignItems="center"
        width="100%"
        height="auto"
        backgroundColor="#fff"
    >

        <Diver maxWidth="1200px" width="90%" width62="95%" marginTop={props.marginTopSec} marginTop92="50px" display="flex" justifyContent="center" alignItems="flex-end" alignItems92={props.alignItems92} flexDir92="column">
              <Diver textAlign="left" textAlign92={props.textAlign} width92="100%" marginRight="50px" marginRight92="0px" marginBottom="105px" marginBottom92="0px">
                <Diver  display="flex" flexDir="column" alignItems={props.alignItems92}>
                  <Diver textAlign="left" textAlign92={props.textAlign}>
                    <ImgGral width={props.widthImg} width62={props.width62Img} height={props.heightImg} src={props.srcImg} alt="service" />
                    <Text fontSize="30px" fontSize62="22.5px" fontWeight="bold" lineHeight="1" textAlign="left" textAlign92={props.textAlign} color="#0047ba" marginTop="15px">{props.titleCard}</Text>
                    <Text fontSize="30px" fontSize62="22.5px" fontWeight="bold" lineHeight="1" textAlign="left" textAlign92={props.textAlign} color="#262626" marginTop="10px">{props.subtitleCard_1}</Text>
                    <Text fontSize="30px" fontSize62="22.5px" fontWeight="bold" lineHeight="1" textAlign="left" textAlign92={props.textAlign} color="#262626">{props.subtitleCard_2}</Text>
                    <Text fontSize="30px" fontSize62="22.5px" fontWeight="bold" lineHeight="1" textAlign="left" textAlign92={props.textAlign} color="#262626">{props.subtitleCard_3}</Text>
                  </Diver>
                </Diver>
              </Diver>
              
              <Diver  width="45%" text textAlign="left" textAlign92={props.textAlign} width92="100%" marginTop92="20px">
                <Text fontSize="22px" fontSize62="16.5px" fontWeight="300" lineHeight="1.18" textAlign="left" textAlign92={props.textAlign} color="#999999">{props.introText}</Text>
                
                <Text fontSize="22px" fontSize62="16.5px" fontWeight="bold" lineHeight="1.09" textAlign="left" textAlign92={props.textAlign} color="#0047ba" marginTop="10px">{props.titleText_1}</Text>
                <Text fontSize="22px" fontSize62="16.5px" lineHeight="1.09" textAlign="left" textAlign92={props.textAlign} color="#808080">{props.descText_1}</Text>
                            
                <Text fontSize="22px" fontSize62="16.5px" fontWeight="bold" lineHeight="1.09" textAlign="left" textAlign92={props.textAlign} color="#0047ba" marginTop="10px">{props.titleText_2}</Text>
                <Text fontSize="22px" fontSize62="16.5px" lineHeight="1.09" textAlign="left" textAlign92={props.textAlign} color="#808080">{props.descText_2}</Text>
                        
                <Text fontSize="22px" fontSize62="16.5px" fontWeight="bold" lineHeight="1.09" textAlign="left" textAlign92={props.textAlign} color="#0047ba" marginTop="10px">{props.titleText_3}</Text>
                <Text fontSize="22px" fontSize62="16.5px" lineHeight="1.09" textAlign="left" textAlign92={props.textAlign} color="#808080">{props.descText_3}</Text>
                        
                  <Text fontSize="24px" fontSize62="18px" fontWeight="bold" lineHeight="0.71" textAlign="left" textAlign92={props.textAlign} color="#262626"  marginTop="30px">¿Estás interesado en este servicio?</Text>
                  <a
                    href={`https://api.whatsapp.com/send?phone=584122743288`}
                    target="_blank"
                    rel="noopener noreferrer"
                    style= {{ width: '100%'} }
                    >
                    <Diver
                        display="flex"
                        width="100%"
                        // maxWidth99="90%"
                        alignItems="center"
                        justifyContent="flex-start"
                        justifyContent92={props.justifyContent92}
                        // marginTop="20px"
                    >
                        <Button
                        text="Obtener más info"
                        fontSize="20px"
                        fontWeight="400"
                        width="220px"
                        width38="176px"
                        widthTele="90%"
                        background="#0046B9"
                        color="#fff"
                        backgroundColorH="#0046B9"
                        colorH="#fff"
                        border="1px solid #0046B9"
                        borderRadius="10px"
                        height="36px"
                        height38="30px"
                        // margin="auto"
                        />
                    </Diver>
                  </a>
              </Diver>
              
        </Diver>
        
        
    </Diver>
    );
};

export default ServiceSection;