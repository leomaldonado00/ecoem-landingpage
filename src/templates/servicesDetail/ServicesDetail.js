import React, { useEffect } from 'react';
import LayoutLanding from '../../components/LayoutLanding';
import ServiceSection from './ServiceSection';
import Forward from './Forward';
import PhraseWspp from '../landing/PhraseWspp';
// import PhraseWspp from './PhraseWspp';

const ServicesDetail = props => {

  useEffect(() => {
    window.scroll(0, 0);
  });

  return (
    <LayoutLanding history={props.history} itemHeader="services">
      <ServiceSection
        titleCard="VENTURE BUILDING"
        subtitleCard_1="Diseño y Validación"
        subtitleCard_2="Modelos de Negocios"
        introText="Centramos nuestros servicios de diseño y validación de modelos de negocio en:"
        titleText_1="Generación de Idea"
        descText_1="Identificamos oportunidades de negocio y co desarrollamos ideas de innovadoras para los mercados digitales y el impacto sistémico."
        titleText_2="Investigación de Mercado"
        descText_2="Diseñamos investigaciones de mercado para conocer las tendencias de los consumidores y las tendencias más importantes en cada industria."
        titleText_3="Estrategia de Marca"
        descText_3="Impulsamos la identidad de los proyectos hasta conseguir los atributos de diferenciación más importantes que dan sentido a su marca."
        srcImg="/assets/images/ventureDet.png"
        widthImg="265px"
        width62Img="197px"
        heightImg="auto"
        marginTopSec="50px"
        alignItems92="flex-start"
        justifyContent92="flex-start"
        textAlign="left"
      />
      <ServiceSection
        titleCard="DIGITAL TRANSFORMATION"
        subtitleCard_1="Diseño y Desarrollo"
        subtitleCard_2="de Productos Digitales"
        introText="Centramos nuestros servicios de diseño y desarrollo de productos digitales  en:"
        titleText_1="Prototipo Inteligente"
        descText_1="Acompañamos a los clientes que necesitan invertir en la primera versión de una prueba de concepto con el desarrollo de prototipo funcional que permita validar con cliente y aliados claves."
        titleText_2="PMV Digital"
        descText_2="Diseñamos y desarrollamos productos digitales bajo un enfoque de Producto Mínimos Viable, que considera todo lo necesario para validar un producto en el mercado de forma controlada."
        titleText_3="Plataforma Digital"
        descText_3="Construimos plataformas digitales con las tecnologías de vanguardia que pueden escalar exponencialmente. Trabajamos con equipos pequeños de programadores dedicados."
        srcImg="/assets/images/digitalDet.png"
        widthImg="265px"
        width62Img="183px"
        heightImg="auto"
        marginTopSec="100px"
        alignItems92="flex-end"
        justifyContent92="flex-end"
        textAlign="right"
      />
      <ServiceSection 
        titleCard="EXPERIENCE DESIGN"
        subtitleCard_1="Diseño y Ejecución"
        subtitleCard_2="de Estrategias"
        subtitleCard_3="de Mercado"
        introText="Centramos nuestros servicios de experience design en:"
        titleText_1="Lanzamiento de Producto"
        descText_1="Conceptualizamos y dirigimos estrategias de lanzamiento de productos digitales que buscan conectar con los usuarios para validar las hipótesis de cada proyecto."
        titleText_2="Ejecución de Marca"
        descText_2="Acompañamos la construcción de contenido y estrategias de mercadeo para lograr el acoplamiento entre el mercado y el producto digital."
        titleText_3="Growth Hacking"
        descText_3="Creamos estrategias de crecimiento exponencial con el uso de BIG DATA y estrategias de Media Buyer para crear conocimiento e inteligencia de mercado."
        srcImg="/assets/images/experienceDet.png"
        widthImg="211px"
        width62Img="185px"
        heightImg="auto"
        marginTopSec="100px"
        alignItems92="flex-start"
        justifyContent92="flex-start"
        textAlign="left"
      />
      <Forward />
      <PhraseWspp backgroundSec="#262626" colorPhrase="#ffffff" paddingBottomSec="70px" paddingTopSec="25px" />
    </LayoutLanding>
  );
};

export default ServicesDetail;
