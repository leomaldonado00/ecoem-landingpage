import React from 'react';
import styled from 'styled-components';
import { FaWhatsapp } from 'react-icons/fa';
import Diver from '../../components/Diver';
import Text from '../../components/Text';
import Button from '../../components/Button';

const PhraseWspp = props => (
  <div id="contact">
    <Diver
      display="flex"
      flexDir="column"
      alignItems="center"
      width="100%"
      height="auto"
      backgroundColor={props.backgroundSec}
      paddingBottom={props.paddingBottomSec}
      paddingTop={props.paddingTopSec}
    >
      <Diver
        display="flex"
        maxWidth="45%"
        textAlign="center"
        maxWidth99="90%"
        marginTop="55px"
      >
        <Text color={props.colorPhrase} fontSize="25px" fontWeight="600">
          Las ideas y el talento están distribuidos pero las oportunidades no!
        </Text>
      </Diver>
      <LinkW
          href={`https://api.whatsapp.com/send?phone=584122743288`}
          target="_blank"
          rel="noopener noreferrer"
        >
      <Diver
        display="flex"
        maxWidth="45%"
        maxWidth99="90%"
        maxWidth62="80%"
        maxWidth38="90%"
        alignItems="center"
        marginTop="20px"
        marginRight="-55px"
      >
          <Button
            text="CHATEA CON NOSOTROS"
            fontSize="20px"
            fontWeight="400"
            width="320px"
            widthTele="90%"
            background="#0046B9"
            color="#fff"
            backgroundColorH="#0046B9"
            colorH="#fff"
            border="1px solid #0046B9"
            borderRadius="10px"
            height="45px"
            height38="40px"
            margin="auto"
          />
          <Diver marginLeft="20px" marginLeft62="5px" marginTop="-15px" cursor="pointer">
            <FaWhatsapp size={50} color="#0046B9" />
          </Diver>
      </Diver>
      </LinkW>
    </Diver>
  </div>
);

const LinkW = styled.a`
width:100%;
display:flex;
justify-content: center;
/* max-width:45%; */
/* max-width:90%; */
align-items:center;
margin-top:20px;
`;

export default PhraseWspp;
