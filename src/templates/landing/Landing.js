import React, { useEffect } from 'react';
import LayoutLanding from '../../components/LayoutLanding';
import PhraseWspp from './PhraseWspp';
import Services from './Services';
import Experience from './Experience';
import Portfolio from './Portfolio';
import Credo from './Credo';
import IntroLanding from './IntroLanding';
import Us from './Us';
import FormLanding from './FormLanding';

const EcoemLanding = props => {

  useEffect(() => {
    if (
      props.history.location.state &&
      props.history.location.state.ancla
    ) {
      setTimeout(() => {
        const element = document.getElementById(
          props.history.location.state.ancla,
        );
        const offset = 1;
        const bodyRect = document.body.getBoundingClientRect().top;
        const elementRect = element.getBoundingClientRect().top;
        const elementPosition = elementRect - bodyRect;
        const offsetPosition = elementPosition - offset;
        window.scrollTo({
          top: offsetPosition,
          behavior: 'smooth'
        });   
        }, 500);
    }
  }, [props.history.location.state]);

  return (
    <LayoutLanding history={props.history} itemHeader={props.history.location.state? props.history.location.state.ancla: null}>
        <IntroLanding />
        <Services />
        <Experience />
        <Portfolio />
        <Us />
        <Credo />
        <PhraseWspp backgroundSec="#fff" colorPhrase="#262626" />
        <FormLanding />
    </LayoutLanding>
  );
};


export default EcoemLanding;
