import React from 'react';
import styled from 'styled-components';
import { AiFillDownCircle } from 'react-icons/ai';
import langStrings from '../../i18n';
import Diver from '../../components/Diver';
import Text from '../../components/Text';

const Us = props => (
    <div id="us">
    <Diver
        display="flex"
        flexDir="column"
        alignItems="center"
        width="100%"
        height="auto"
        backgroundColor="#fff"
    >

        <Diver display="flex" justifyContent="space-between" justifyContent92="center" flexDir92="column" maxWidth="1200px" width="90%" width92="95%" marginTop="20px">
            <Diver width="45%" width92="100%" textAlign="left">
                    <Text fontSize="20px" fontSize62="28px" fontWeight="bold" color="#0046B9" textAlign="left" marginBottom="20px">{langStrings.history}</Text>
                <Tittle color="#262626" textAlign="left" fontWeight="bold" display="inline">ECOEM nace </Tittle>
                <Tittle color="#262626" textAlign="left" fontWeight="normal" display="inline">como un proyecto que buscaba anticiparse a las realidades globales de los paises emergentes y apostar al desarrollo del capital creativo. Somos un grupo de más de 60 emprendedores y talentos que nos organizamos bajo la modalidad de un estudio de capital creativo y buscamos impulsar proyectos que integran el uso de las tecnologías de información con experiencias de usuario significativas. Con nuestros seis años de fundación, tenemos una estructura de operaciones que nos permite trabajar para toda Iberoamérica.  </Tittle>
            </Diver>
            <Diver width="45%" width92="100%" textAlign="left" marginTop92="25px">
                <Text fontSize="20px" fontSize62="28px" fontWeight="bold" color="#0046B9" textAlign="left" marginBottom="20px">{langStrings.networks}</Text>
                <Text fontSize="25px" fontSize62="28px" fontWeight="bold" color="#252525" textAlign="left" lineHeight="1.33">SEEDSTARS/</Text>
                <Text fontSize="25px" fontSize62="28px" fontWeight="bold" color="#252525" textAlign="left" lineHeight="1.33">WESTERWELLE/</Text>
                <Text fontSize="25px" fontSize62="28px" fontWeight="bold" color="#252525" textAlign="left" lineHeight="1.33">BIME/</Text>
                <Text fontSize="25px" fontSize62="28px" fontWeight="bold" color="#252525" textAlign="left" lineHeight="1.33">GCL ALUMNI/</Text>
                <Text fontSize="25px" fontSize62="28px" fontWeight="bold" color="#252525" textAlign="left" lineHeight="1.33">SOLIDUS/</Text>
                <Text fontSize="25px" fontSize62="28px" fontWeight="bold" color="#252525" textAlign="left" lineHeight="1.33">GSF/</Text>
                <Text fontSize="25px" fontSize62="28px" fontWeight="bold" color="#252525" textAlign="left" lineHeight="1.33">ESC+G/</Text>
            </Diver>
        </Diver>

        
        <Diver marginTop="30px" marginBottom="30px">
            <AiFillDownCircle size={30} color="#0046B9" />
        </Diver>

    </Diver>

    </div>
);

const Tittle = styled.h3`
font-size: 20px;
color: ${props => props.color || "#1a1a1a" };
margin: 0px 0px 0px 0px;
text-align: ${props => props.textAlign || "#center" }; 
font-weight:${props => props.fontWeight || "normal" };
display:${props => props.display || "block" };
font-stretch: normal;
  font-style: normal;
  line-height: 1.2;
  letter-spacing: 0.2px;
`;

export default Us;
