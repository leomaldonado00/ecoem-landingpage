import React from 'react';
import Diver from '../../components/Diver';
import PortfolioVenture from './PortfolioVenture';
import PortfolioDigital from './PortfolioDigital';
import PortfolioBrands from './PortfolioBrands';

const Portfolio = props => (
  <div id="portfolio">
      <Diver marginBottom="50px" width="100%">
        <PortfolioVenture />
        <PortfolioDigital />
        <PortfolioBrands />
      </Diver>
  </div>
);

export default Portfolio;
