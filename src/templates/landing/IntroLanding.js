import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { AiFillDownCircle } from 'react-icons/ai';
import { langCode } from '../../ducks/i18n';
import langStrings from '../../i18n';
import Diver from '../../components/Diver';
import Text from '../../components/Text';
import Button from '../../components/Button';

const IntroLanding = props => {

  const langCodeT = useSelector(state => langCode(state));
  useEffect(() => {
  }, [langCodeT]);

  return (
  <div id="intro">
    <Diver
      display="flex"
      flexDir="column"
      alignItems="center"
      width="100%"
      height="auto"
      backgroundColor="#262626"
    >
      <Diver
        backgroundColor="#262626"
        backgroundImg="url(/assets/images/banner1.png)"
        backgroundPos="left 0px"
        backgroundRep="no-repeat"
        backgroundSize="contain"
        // backgroundSize62="cover"
        width="90%"
        maxWidth="1200px"
        minHeight="70vh"
        minHeight62="50vh"
        display="flex"
        // flexDir="column"
        alignItems="center"
        justifyContent="space-between"
      >
        <WrapText>
          {/* <TitleBanner display="block" color="#0046B9">
            Ecoem es
              </TitleBanner> */}
          <TextBanner display="block" color="#ffffff">
            {langStrings.we_are}
          </TextBanner>
          <Diver display="block" textAlign="left">
            <TextBanner display="inline" color="#ffffff">
            {langStrings.focused_on}{' '}
            </TextBanner>

            <Diver display="inline" 
            // backgroundImg="url(/assets/images/linehorizontal.png)" backgroundSize="contain" backgroundPos="center" 
            >
              <TextBanner display="inline" color="#ffffff" textDeco="line-through #0046B9">
              {langStrings.investment}
              </TextBanner>
            </Diver>

            <TextBanner display="inline" color="#ffffff">
            {' '}
            </TextBanner>

            <TextBanner display="inline-block" color="#0046B9" >
            {langStrings.cocreation}
            </TextBanner>
          </Diver>
          <TextBanner display="block" color="#ffffff">
          {langStrings.in_tecnology}
          </TextBanner>
          <TextBanner display="block" color="#ffffff">
          {langStrings.and_systemic}
          </TextBanner>
        </WrapText>
        <Diver
          display="flex"
          flexDir="column"
          alignItems="center"
          alignSelf="flex-end"
          display92="none"
        >
          <Text fontSize="20px" fontWeight="bold" color="#ffffff">
            ¿Tienes una idea increíble?
          </Text>

          <a
            href={`https://api.whatsapp.com/send?phone=584122743288`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <Diver marginTop="15px" width="100%">
              {' '}
              {/* width="100%" */}
              <Button
                text="¡CUÉNTANOS AQUÍ!"
                fontSize="15px"
                fontWeight="600"
                width="186px"
                widthTele="90%"
                marginTop="10px"
                background="#ffffff"
                color="#262626"
                backgroundColorH="#ffffff"
                colorH="#262626"
                border="1px solid #ffffff"
                borderRadius="10px"
                height="32px"
                // height38="45px"
                margin="auto"
                marginDiv="auto"
              />
            </Diver>
          </a>
        </Diver>
      </Diver>

      <Diver
        display="none"
        flexDir="column"
        alignItems="center"
        display92="flex"
        marginTop="60px"
      >
        <Text fontSize="20px" fontWeight="bold" color="#0046B9">
          ¿Tienes una idea increíble?
        </Text>

        <a
          href={`https://api.whatsapp.com/send?phone=584122743288`}
          target="_blank"
          rel="noopener noreferrer"
          style= {{ width: '100%'} }
        >
          <Diver marginTop="15px" width="100%">
            {' '}
            {/* width="100%" */}
            <Button
              text="¡CUÉNTANOS AQUÍ!"
              fontSize="15px"
              fontWeight="600"
              width="186px"
              widthTele="90%"
              marginTop="10px"
              background="#ffffff"
              color="#0046B9"
              backgroundColorH="#ffffff"
              colorH="#0046B9"
              border="1px solid #ffffff"
              borderRadius="10px"
              height="32px"
              // height38="45px"
              margin="auto"
              marginDiv="auto"

            />
          </Diver>
        </a>
      </Diver>

      <Diver marginTop="30px" marginBottom="30px">
        <AiFillDownCircle size={30} color="#0046B9" />
      </Diver>
    </Diver>
  </div>
  );
};

const WrapText = styled.div`
  width: 100%;
  text-align: left;
  margin-left: 30px;
  margin-bottom: -20px;
  /* 
@media (max-width: 1035px) {
    width:70%;
}
@media (max-width: 1035px) {
    width:85%;
}
*/
@media (max-width: 780px) {
  margin-bottom: 0px;
}
  @media (max-width: 620px) {
    margin-left: 0px;
  }
  @media (max-width: 355px) {
    margin-bottom: 30px;
  } 
`;
/*
const TitleBanner = styled.div`
  font-size: 44.5px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1;
  letter-spacing: 0.33px;
  text-align: left;
  color: ${props => props.color || '#1a1a1a'};
  display: ${props => props.display || 'block'};
  @media (max-width: 780px) {
    font-size: 35px;
  }
  @media (max-width: 520px) {
    font-size: 30px;
  }
`;
*/
const TextBanner = styled.div`

background-image:  ${props => props.backgroundImg || 'none'};
background-size: ${props => props.backgroundSize || 'auto auto'};
background-position: ${props => props.backgroundPos || '0% 0%'};
padding-left:  ${props => props.paddingLeft || 'none'};

text-decoration:${props => props.textDeco || 'none'};

  font-size: 33px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1;
  letter-spacing: 0.33px;
  text-align: left;
  color: ${props => props.color || '#1a1a1a'};
  display: ${props => props.display || 'block'};
  @media (max-width: 780px) {
    font-size: 25px;
  }
  @media (max-width: 620px) {
    font-size: 18px;
  }
  @media (max-width: 358px) {
    font-size: 16px;
  }
  @media (max-width: 328px) {
    font-size: 15px;
  }

`;
export default IntroLanding;
