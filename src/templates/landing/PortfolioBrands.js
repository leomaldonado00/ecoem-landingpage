import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import Slider from 'react-slick';
import { langCode } from '../../ducks/i18n';
import langStrings from '../../i18n';
import ImgGral from '../../components/ImgGral';
import Diver from '../../components/Diver';
import Text from '../../components/Text';
import CardPortfolio from '../../components/CardPortfolio';

const PortfolioBrands = (props) => {

  const langCodeT = useSelector(state => langCode(state));
  useEffect(() => {
  }, [langCodeT]);

  function SampleNextArrow(props) {
    const {  style, onClick } = props;
    return (
      <div
      style={{ ...style, color:"#0047ba", cursor:"pointer"}}
      onClick={onClick}
      >
       <ImgGral width="42px" height="42px" width62="25px" height62="35px" src="/assets/images/nextslider.png" alt="next" cursor="pointer" />

    </div>
    );
  }
  
  function SamplePrevArrow(props) {
    const {  style, onClick } = props;
    return (
      <div
      style={{ ...style, color:"#0047ba", cursor:"pointer"}}
      onClick={onClick}
      >
       <ImgGral width="42px" height="42px" width62="25px" height62="35px" src="/assets/images/prevslider.png" alt="prev" cursor="pointer" />
      </div>
    );
  }


  
  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    nextArrow: <SampleNextArrow size={70}  color={"#0047ba"} />,
      prevArrow: <SamplePrevArrow size={70}  color={"#0047ba"} />,

    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: false,
          dots: false,
          speed: 500,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: false,
          dots: false,
          speed: 500,
        }
      },
    ]

  };


    return (
        <Diver
        display="flex"
        flexDir="column"
        alignItems="center"
        width="100%"
        height="auto"
        backgroundColor="#fff"
        marginTop="30px"
      >
        <Diver maxWidth="1200px" width="90%" width62="95%" marginTop="20px" marginBottom="20px" display="flex" alignItems="flex-start" justifyContent="space-between" flexDir48="column">
          <Diver>
              <Text fontSize="20px" fontSize62="23px" fontWeight="bold" color="#0046B9" textAlign="left" display="inline">{langStrings.portfolio}: </Text>
              <Text fontSize="20px" fontSize62="23px" fontWeight="bold" color="#808080" textAlign="left" display="inline">BRANDS + CONTENT</Text>
          </Diver>
          {/* <Diver display="flex" alignItems="center" >
                  <Description>Ver todos</Description>
                  <Diver marginLeft="15px" cursor="pointer" height="15px" width="15px" display="flex" justifyContent="center" alignItems="center">
                    <ImgNext src="/assets/images/nextlight.png" alt="next" />
                  </Diver>
          </Diver> */}
  
        </Diver>
  
        <Diver maxWidth="1200px" width="90%" width62="95%">
        <Slider {...settings}>
        <div>
          <CardPortfolio imageProject={null} title="Tidbit― " description="Compañía con app móvil que provee a sus usuarios con ratings personalizados de restaurantes, basados en sus redes sociales o personas que escogen seguir. " page="Tidbitsocial.com" link="http://www.tidbitsocial.com/" srcImg="/assets/images/tid_portfolio.jpg" />
          </div>
          <div>
          <CardPortfolio imageProject={null} title="Joga― " description="Joga Fantasy, plataforma que permite jugar fantasy de fútbol en la Liga MX , jugar fútbol de verdad!" srcImg="/assets/images/joga2_portfolio.jpg" />
          </div>
          <div>
          <CardPortfolio imageProject={null} title="GSF― " description="Impulsamos el desarrollo de organizaciones para que ocupen posiciones de liderazgo en su sector de negocio y región de desarrollo. " page="gsfsmart.com" link="https://www.gsfsmart.com/" srcImg="/assets/images/gsf_portfolio.jpg" />
          </div>
          <div>
          <CardPortfolio imageProject={null} title="Free Convict― " description="Grupo de presidiarios y expresidiarios dispuestos a generar un cambio a través de la música." page="@freeconvict" link="https://www.instagram.com/freeconvict/?hl=es-la" srcImg="/assets/images/freec_portfolio.jpg" />
          </div>
          <div>
          <CardPortfolio imageProject={null} title="Pasefit― " description="es una app con miles de clases deportivas en un solo pase. Uniendo el mundo deportivo con el digital. " page="@pase.fit" link="https://www.instagram.com/pase.fit/?hl=es-la" srcImg="/assets/images/pasefit2_portfolio.jpg" />
          </div>
          <div>
          <CardPortfolio imageProject={null} title="Innotica― " description="Empresa dedicada al desarrollo de proyectos para la automatización de hogares, edificios y ciudades. " page="Innotica.net" link="https://innotica.net/" srcImg="/assets/images/inno_portfolio.jpg" />
          </div>
        </Slider>
      </Diver>
      
      </Diver>
      );
};

/* 
const ImgNext = styled.img`
  width: 15px;
  height: 15px;
`;
const Description = styled.p`
  font-size: 15px;
  color: #999999;
  margin: 0;

  border-radius: 3px;
  text-align: center;

  cursor: pointer;
`;
const WidthDefault = styled.div`
  width: 100%;
  display:flex;
  max-width: 1200px;
  @media (max-width: 1200px) {
    width: 90vw;
  }
`;

const Arrow = styled.div`
display: block;
color: #0047ba;
font-size: 70px;
size: letter;
cursor: pointer;
  @media (max-width: 920px) {
    font-size: 50px;
  }
  @media (max-width: 620px) {
    font-size: 40px;
  }

`;
*/


/* 
const PortfolioCards = styled.div`
  width: 100%;
  max-width: 1200px;
  // min-height: 40vh; //////////////////
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  justify-items: center;
  // background: red; //////////////////
  padding-top: 5vh;

  @media (max-width: 1070px) {
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 40px;
  }
  @media (max-width: 620px) {
    grid-template-columns: 1fr;
  }
`;
*/

export default PortfolioBrands;