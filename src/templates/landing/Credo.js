import React from 'react';
import styled from 'styled-components';
import Diver from '../../components/Diver';
import Text from '../../components/Text';

const Credo = props => (
    <div id="credo">
    <Diver
        display="flex"
        flexDir="column"
        alignItems="center"
        width="100%"
        height="auto"
        backgroundColor="#0046B9"
    >
        <Diver maxWidth="1200px" width="90%" width62="95%" marginTop="60px" display="flex" justifyContent="flex-start">
            <ImgGral width="auto" height="35px" src="/assets/images/logocredo.png" alt="credologo" />
        </Diver>
        <Diver maxWidth="1200px" width="90%" width62="95%" marginTop="10px">
            <Text fontSize="20px" fontWeight="normal" color="#ffffff" textAlign="left">Nuestra filosofía gira entorno al:</Text>
        </Diver>
        <WrapMaps display="flex" justifyContent="center" maxWidth="1200px" width="90%" marginTop="20px" alignItems="center">
            <Diver marginRight="15px" marginRight92="0">
                <ImgGral width="311px" width62="280px" height="auto" src="/assets/images/mentalmap.png" alt="credo" />
            </Diver>
            <Diver display="flex" justifyContent="flex-end" justifyContent92="center" alignItems="center" maxWidth="50%" maxWidth92="none" marginTop92="40px">
                <Diver width="auto" marginRight="10px">
                    <Text fontSize="27.5px" fontSize62="19px" fontWeight="bold" fontStretch="normal" lineHeight="1" letterSpacing="0.28px" color="#ffffff" textAlign="right">Trabajamos con un foco en el desarrollo de proyectos para toda Iberoamérica</Text>
                </Diver>
                <Diver>
                    <ImgGral width="311px" width62="170px" height="auto" src="/assets/images/mapamundi.png" alt="credo" />
                </Diver>
            </Diver>
        </WrapMaps>

    </Diver>

    </div>
);

const WrapMaps = styled.div`
display: flex;
justify-content:center;
max-width:1200px;
width:90%;
margin-top:20px;
margin-bottom:60px;
align-items:center;
@media (max-width: 1095px) {
    justify-content:space-between;
}
@media (max-width: 920px) {
    flex-direction: column;
}
@media (max-width: 620px) {
    width:95%;
}

`;
const ImgGral = styled.img`
width: ${props => props.width || "auto" };
height: ${props => props.height || "auto" };

@media (max-width: 620px) {
    width: ${props => props.width62 || props.width72 || props.width76 || props.width92 || props.width99 ||  props.width10 ||  props.width12 || props.width || 'auto'};
    height: ${props => props.height62 || props.height72 || props.height76 || props.height92 || props.height99 ||  props.height10 ||  props.height12 || props.height || 'auto'};
}

`;

export default Credo;
