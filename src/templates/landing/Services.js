import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { AiFillDownCircle } from 'react-icons/ai';
// import { FaWhatsapp } from 'react-icons/fa';
import { langCode } from '../../ducks/i18n';
import langStrings from '../../i18n';
import Diver from '../../components/Diver';
import Text from '../../components/Text';
import ColumnWrap from '../../components/ColumnWrap';
import CardServices from '../../components/CardServices';

const Services = props => {

    const langCodeT = useSelector(state => langCode(state));
    useEffect(() => {
    }, [langCodeT]);
  
    return (
    <div id="services">
    <Diver
        display="flex"
        flexDir="column"
        alignItems="center"
        width="100%"
        height="auto"
        backgroundColor="#fff"
        marginTop="50px"
    >
        <Diver maxWidth="1200px" width="90%" width62="95%">
          <Text fontSize="20px" fontSize62="28px" fontWeight="bold" color="#0046B9" textAlign="left">{langStrings.services}</Text>
        </Diver>

        <ColumnWrap
          col="1fr 1fr 1fr"
          colMd="1fr 1fr"
          colSm="1fr"
          maxWidth="1200px"
          justifyItems="center"ç        
          alignItems="end"
          marginTop="40px"
        >
            <CardServices 
            title="VENTURE BUILDING"
            subtitleT="Diseño y Validación"
            subtitleB="Modelo de Negocios"
            description="Ver todos"
            img="/assets/images/imgservice1.png" 
            widthImg="191px"
            heightImg="174px"
            widthImg62="144px"
            heightImg62="132px" 
            flexDir62="row-reverse"
            alignItems62="flex-start" 
            textAlign62="left"
            />
            <CardServices 
            title="DIGITAL TRANSFORMATION"
            subtitleT="Diseño y Desarrollo"
            subtitleB="de Productos Digitales"
            description="Ver todos"
            img="/assets/images/imgservice2_2.png" 
            widthImg="201px"
            heightImg="266px" 
            widthImg62="145px"
            heightImg62="191px" 
            flexDir62="row"
            alignItems62="flex-end" 
            textAlign62="right"
            />
            <ColumnPosition>
                <CardServices 
                title="EXPERIENCE DESING"
                subtitleT="Diseño y Ejecución"
                subtitleB="de Estrategias de Mercado"
                description="Ver todos"
                img="/assets/images/imgservice3.png" 
                widthImg="154px"
                heightImg="189px" 
                widthImg62="113px"
                heightImg62="139px"     
                flexDir62="row-reverse"
                alignItems62="flex-start" 
                textAlign62="left"
                />
                </ColumnPosition>
        </ColumnWrap>
        <Diver marginTop="30px">
            <AiFillDownCircle size={30} color="#0046B9" />
        </Diver>

    </Diver>

    </div>
    );
};

const ColumnPosition = styled.div`
  @media (max-width: 992px) {
    grid-column: 1 / 4;
  }
  @media (max-width: 768px) {
    grid-column: initial;
  }
  @media (max-width: 620px) {
  width: 95%;
}

`;
export default Services;
