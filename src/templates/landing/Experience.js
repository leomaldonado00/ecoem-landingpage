import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { AiFillDownCircle } from 'react-icons/ai';
// import { FaWhatsapp } from 'react-icons/fa';
import { langCode } from '../../ducks/i18n';
import langStrings from '../../i18n';
import Diver from '../../components/Diver';
import Text from '../../components/Text';

const Experience = props => {

    const langCodeT = useSelector(state => langCode(state));
    useEffect(() => {
    }, [langCodeT]);

    return (
    <div id="experience">
    <Diver
        display="flex"
        flexDir="column"
        alignItems="center"
        width="100%"
        height="auto"
        backgroundColor="#fff"
    >
        <Diver maxWidth="1200px" width="90%" width62="95%" marginTop="20px">
            <Text fontSize="20px" fontSize62="28px" fontWeight="bold" color="#0046B9" textAlign="left">{langStrings.experience}</Text>
        </Diver>

        <Diver display="flex" justifyContent="space-between" justifyContent92="center" flexDir92="column" maxWidth="1200px" width="90%" width92="95%" marginTop="20px">
            <Diver width="45%" textAlign="left" width92="100%">
                <Tittle color="#262626" textAlign="left" fontWeight="bold">Trabajamos en todas las industrias interconectando tecnologías emergentes y tendencias culturales.</Tittle>
                <Tittle color="#0046B9" textAlign="left" fontWeight="normal" marginTop92="15px">"Nacimos del emprendimiento y entendemos lo complejo que es desarrollar las ideas y crear empresas que logren atravesar el valle de la muerte. Acumulamos una experiencia multidisciplinaria que nos permite acompañar a cualquier proyecto, desde la fase de desarrollo en donde se encuentre."</Tittle>
            </Diver>
            <Diver width="45%" textAlign="left" width92="100%">
                <Tittle color="#0046B9" textAlign="left" fontWeight="normal" display="inline">Nos adaptamos de manera flexible a la realidad de cada proyecto y- </Tittle>
                <Tittle color="#0046B9" textAlign="left" fontWeight="bold" display="inline">comprendemos que el manejo del error es la herramienta más importante para superar las barreras del emprendimiento. </Tittle>            
                <Tittle color="#0046B9" textAlign="left" fontWeight="normal" display="inline">Las estadísticas son rudas, sólo 6% de los emprendimientos logran validarse en los mercados. Aquí compartimos alguna de las historias que hemos vivido en los últimos seis años."</Tittle>
                <Diver textAlign="left" marginTop="15px">
                    <Text color="#262626" fontSize="12px" fontWeight="bold" textAlign="left" display="inline">ECOEM </Text>
                    <Text color="#262626" fontSize="12px" fontWeight="normal" textAlign="left" display="inline">| Dirección de Innovación estratégica</Text>

                </Diver>
            </Diver>
        </Diver>

        
        <Diver marginTop="30px">
            <AiFillDownCircle size={30} color="#0046B9" />
        </Diver>

    </Diver>

    </div>
    );

};

const Tittle = styled.h3`
font-size: 20px;
color: ${props => props.color || "#1a1a1a" };
margin: 0px 0px 0px 0px;
text-align: ${props => props.textAlign || "#center" }; 
font-weight:${props => props.fontWeight || "normal" };
display:${props => props.display || "block" };
font-stretch: normal;
  font-style: normal;
  line-height: 1.2;
  letter-spacing: 0.2px;

  @media (max-width : 920px) {
    margin-top: ${props => props.marginTop92 || "0px" };
};

`;

export default Experience;
