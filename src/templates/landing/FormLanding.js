import React, { useState } from 'react';
import styled from 'styled-components';
import { AiFillDownCircle } from 'react-icons/ai';
import langStrings from '../../i18n';
import Diver from '../../components/Diver';
import Button from '../../components/Button';

const FormLanding = props => {

  const [formLogin, setFormLogin] = useState({
    email: ``,
    phone: ``,
    country: ``,
    project_name: ``,
    description: ``,
    relation: ``,
    Eemail: '',
    Ephone: '',
    Ecountry: ``,
    Eproject_name: ``,
    Edescription: ``,
    Erelation: ``,
    });

    const validate = dataLogin => {
      let errores = {};
      if (
        !dataLogin.email ||
        !dataLogin.phone ||
        !dataLogin.country ||
        !dataLogin.project_name ||
        !dataLogin.description ||
        !dataLogin.relation
      ) {
        if (dataLogin.email.length === 0) {
          errores = {
            ...errores,
            Eemail: langStrings.errorRequired,
            Eflag: true,
          };
        }
        if (dataLogin.phone.length === 0) {
          errores = {
            ...errores,
            Ephone: langStrings.errorRequired,
            Eflag: true,
          };
        }
        if (dataLogin.country.length === 0) {
          errores = {
            ...errores,
            Ecountry: langStrings.errorRequired,
            Eflag: true,
          };
        }
        if (dataLogin.project_name.length === 0) {
          errores = {
            ...errores,
            Eproject_name: langStrings.errorRequired,
            Eflag: true,
          };
        }
        if (dataLogin.description.length === 0) {
          errores = {
            ...errores,
            Edescription: langStrings.errorRequired,
            Eflag: true,
          };
        }
        if (dataLogin.relation.length === 0) {
          errores = {
            ...errores,
            Erelation: langStrings.errorRequired,
            Eflag: true,
          };
        }
        
        setFormLogin({ ...dataLogin, ...errores });
        return false;
      } 

      if (
        dataLogin.email.length !== 0 &&
        !/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i.test(dataLogin.email)
      ) {
        errores = {
          ...errores,
          Eemail: langStrings.validEmail,
          Eflag: true,
        };
        setFormLogin({ ...dataLogin, ...errores });
        return false;
      }
      if (
        dataLogin.phone.length !== 0 &&
        !/^[\d+]{7,17}$/.test(dataLogin.phone) //   ^[\d/+]{1,17}$
      ) {
        errores = {
          ...errores,
          Ephone:
            'Verifique que el número telefónico sea correcto, de 7 a 17 caracteres numéricos.',
          Eflag: true,
        };
        setFormLogin({ ...dataLogin, ...errores });
        return false;
      }
      if (
        dataLogin.phone.length !== 0 &&
        !/^\d(?!.*(0)\1{3})/gm.test(dataLogin.phone) //   ^[\d/+]{1,17}$
      ) {
        errores = {
          ...errores,
          Ephone:
            'Verifique que el número telefónico sea correcto, de 7 a 17 caracteres numéricos.',
          Eflag: true,
        };
        setFormLogin({ ...dataLogin, ...errores });
        return false;
      }
      if (
        dataLogin.country.length !== 0 &&
        !/^(([A-Za-zñÑáÁéÉíÍóÓúÚç]+[\s]*)+){3,40}$/.test(
          dataLogin.country,
        )
      ) {
        errores = {
          ...errores,
          Ecountry: langStrings.validCountry,
          Eflag: true,
        };
        setFormLogin({ ...dataLogin, ...errores });
        return false;
      }
      if (
        dataLogin.project_name.length !== 0 &&
        !/^(.){3,50}$/.test(dataLogin.project_name)
      ) {
        errores = {
          ...errores,
          Eproject_name: 'Nombre del proyecto debe tener de 3 a 50 caracteres.',
          Eflag: true,
        };
        setFormLogin({ ...dataLogin, ...errores });
        return false;
      }
      if (
        dataLogin.description.length !== 0 &&
        !/^(.){3,90}$/.test(dataLogin.description)
      ) {
        errores = {
          ...errores,
          Edescription: 'La descripción debe tener de 3 a 90 caracteres.',
          Eflag: true,
        };
        setFormLogin({ ...dataLogin, ...errores });
        return false;
      }
      if (dataLogin.relation.length !== 0 && !/^(.){3,70}$/.test(dataLogin.relation)) {
        errores = {
          ...errores,
          Erelation: 'El campo debe tener de 3 a 70 caracteres.',
          Eflag: true,
        };
        setFormLogin({ ...dataLogin, ...errores });
        return false;
      }
  
      setFormLogin({ ...dataLogin, Eflag: false });
      return true;
  
    };  

    const escrib = e => {
      const regx = / +/g;
      if (!/^\s/.test(e.target.value)) {
        if (e.target.name === 'email') {
          if (e.target.value.length <= 60) {
            setFormLogin({
              ...formLogin,
              [e.target.name]: e.target.value.replace(regx, ' '),
              [`E${e.target.name}`]: '',
            });
          }
        }
        if (e.target.name === 'phone') {
          if (e.target.value.length <= 17) {
            setFormLogin({
              ...formLogin,
              [e.target.name]: e.target.value.replace(regx, ' '),
              [`E${e.target.name}`]: '',
            });
          }
        }
        if (e.target.name === 'country') {
          if (e.target.value.length <= 40) {
            setFormLogin({
              ...formLogin,
              [e.target.name]: e.target.value.replace(regx, ' '),
              [`E${e.target.name}`]: '',
            });
          }
        }
        if (e.target.name === 'project_name') {
          if (e.target.value.length <= 50) {
            setFormLogin({
              ...formLogin,
              [e.target.name]: e.target.value.replace(regx, ' '),
              [`E${e.target.name}`]: '',
            });
          }
        }
        if (e.target.name === 'description') {
          if (e.target.value.length <= 90) {
            setFormLogin({
              ...formLogin,
              [e.target.name]: e.target.value.replace(regx, ' '),
              [`E${e.target.name}`]: '',
            });
          }
        }
        if (e.target.name === 'relation') {
          if (e.target.value.length <= 70) {
            setFormLogin({
              ...formLogin,
              [e.target.name]: e.target.value.replace(regx, ' '),
              [`E${e.target.name}`]: '',
            });
          }
        }

      }
    };
  
    const validate0 = dataLogin => {
      const valid = validate(dataLogin);
      const { email, phone, country, project_name, description, relation } = dataLogin;
      const dataLoginF = { email, phone, country, project_name, description, relation };
      if (valid) {
        console.log("VALID",dataLoginF);
      } else {
        console.log("NO VALID",formLogin);
      }
    };
  
  return (
  <div id="form-landing">
    <Diver
      display="flex"
      flexDir="column"
      alignItems="center"
      width="100%"
      height="auto"
      backgroundColor="#fff"
    >
      <Diver
        display="flex"
        justifyContent="center"
        alignItems="center"
        flexDir="column"
        maxWidth="1200px"
        width="90%"
        width92="95%"
        marginTop="30px"
      >
        <Input type="text" name="email" placeholder="CORREO" onChange={e => escrib(e)} value={formLogin.email} />
        {formLogin.Eemail.length !==0 ? ( <Error>{formLogin.Eemail}</Error> ) : null}
        <Input type="text" name="phone" placeholder="TELÉFONO" onChange={e => escrib(e)} value={formLogin.phone} />
        {formLogin.Ephone.length !==0 ? ( <Error>{formLogin.Ephone}</Error> ) : null}
        <Input type="text" name="country" placeholder="PAÍS" onChange={e => escrib(e)} value={formLogin.country} />
        {formLogin.Ecountry.length !==0 ? ( <Error>{formLogin.Ecountry}</Error> ) : null}
        <Input
          type="text"
          name="project_name"
          placeholder="NOMBRE DEL PROYECTO"
          onChange={e => escrib(e)} value={formLogin.project_name}
        />
        {formLogin.Eproject_name.length !==0 ? ( <Error>{formLogin.Eproject_name}</Error> ) : null}
        <Input type="text" name="description" placeholder="DESCRIPCIÓN" onChange={e => escrib(e)} value={formLogin.description} />
        {formLogin.Edescription.length !==0 ? ( <Error>{formLogin.Edescription}</Error> ) : null}
        <Input
          type="text"
          name="relation"
          placeholder="¿CUÁL ES TU RELACIÓN CON LA IDEA?"
          onChange={e => escrib(e)} value={formLogin.relation}
        />
        {formLogin.Erelation.length !==0 ? ( <Error>{formLogin.Erelation}</Error> ) : null}
        <Diver
          display="flex"
          width="100%"
          maxWidth99="90%"
          alignItems="center"
          marginTop="20px"
        >
          <Button
            text="ENVIAR"
            fontSize="20px"
            fontWeight="400"
            width="180px"
            widthTele="90%"
            background="#0046B9"
            color="#fff"
            backgroundColorH="#0046B9"
            colorH="#fff"
            border="1px solid #0046B9"
            borderRadius="10px"
            height="36px"
            height38="30px"
            margin="auto"
            marginDiv="auto"
            onClick={() => validate0(formLogin)}
          />
        </Diver>

        <Diver marginTop="30px" marginBottom="30px">
          <AiFillDownCircle size={30} color="#0046B9" />
        </Diver>
      </Diver>
    </Diver>
  </div>
  );

};
const Input = styled.input`
font-family: Source Sans Pro;
font-size: 18px;
font-weight: 300;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.57;
  letter-spacing: 0.21px;

width: 760px;
  height: 30px;
  border: 0;

  border-radius: 12px;
  background-color: #f7f8fb;
  margin: 0;
  margin-bottom: 10px;
  padding-left: 25px;
  outline: none;
  color: #999999;

  ::placeholder {
    color: #999999;
  }

  @media (max-width: 800px) {
    width: calc(100% - 25px);
  }
`;
const Error = styled.div`
  font-family: Source Sans Pro;
  color: red;
  font-size: 14px;
  font-weight: normal;
  text-align: center;
  margin-bottom: 10px;
`;

export default FormLanding;
