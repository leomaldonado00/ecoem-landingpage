import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import Slider from 'react-slick';
import { langCode } from '../../ducks/i18n';
import langStrings from '../../i18n';
import Diver from '../../components/Diver';
import ImgGral from '../../components/ImgGral';
import Text from '../../components/Text';
import CardPortfolio from '../../components/CardPortfolio';

const PortfolioVenture = props => {

  const langCodeT = useSelector(state => langCode(state));
  useEffect(() => {
  }, [langCodeT]);

  function SampleNextArrow(props) {
    const {  style, onClick } = props;
    return (
      <div
      style={{ ...style, color:"#0047ba", cursor:"pointer"}}
      onClick={onClick}
      >
       <ImgGral width="42px" height="42px" width62="25px" height62="35px" src="/assets/images/nextslider.png" alt="next" cursor="pointer" />

    </div>
    );
  }
  
  function SamplePrevArrow(props) {
    const {  style, onClick } = props;
    return (
      <div
      style={{ ...style, color:"#0047ba", cursor:"pointer"}}
      onClick={onClick}
      >
       <ImgGral width="42px" height="42px" width62="25px" height62="35px" src="/assets/images/prevslider.png" alt="prev" cursor="pointer" />
      </div>
    );
  }



  
  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    nextArrow: <SampleNextArrow size={70}  color={"#0047ba"} />,
      prevArrow: <SamplePrevArrow size={70}  color={"#0047ba"} />,

    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: false,
          dots: false,
          speed: 500,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: false,
          dots: false,
          speed: 500,
        }
      },
    ]

  };


  return (
    <Diver
      display="flex"
      flexDir="column"
      alignItems="center"
      width="100%"
      height="auto"
      backgroundColor="#fff"
    >
      <Diver
        maxWidth="1200px"
        width="90%"
        width62="95%"
        marginTop="20px"
        marginBottom="20px"
        display="flex"
        alignItems="flex-start"
        justifyContent="space-between"
        // flexDir48="column"
      >

        <Diver>
          <Text
            fontSize="20px"
            fontSize62="23px"
            fontWeight="bold"
            color="#0046B9"
            textAlign="left"
            display="inline"
          >
            {langStrings.portfolio}:{' '}
          </Text>
          <Text
            fontSize="20px"
            fontSize62="23px"
            fontWeight="bold"
            color="#808080"
            textAlign="left"
            display="inline"
          >
            VENTURE + IMPACT
          </Text>
        </Diver>
        {/* <Diver display="flex" alignItems="center">
          <Description>Ver todos</Description>
          <Diver
            marginLeft="15px"
            cursor="pointer"
            height="15px"
            width="15px"
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <ImgNext src="/assets/images/nextlight.png" alt="next" />
          </Diver>
  </Diver> */}
      </Diver>

      


<Diver maxWidth="1200px" width="90%" width62="95%">
        <Slider {...settings}>
          <div>
          <CardPortfolio imageProject={null} title="Arepa Music― " description="Plataforma de desarrollo de proyectos musicales dirigida de manera exclusiva a la venta y promoción de música hecha por venezolanos de todos los géneros en todo el mundo. " page="Arepamusic.com" link="http://arepamusic.com/" srcImg="/assets/images/arepamusic.jpg" />
          </div>
          <div>
          <CardPortfolio imageProject={null} title="Arawato― " description="Digitalizando los procesos de una idea de negocio a través de la creación de soluciones digitales que impulsen su crecimiento. " page="Arawato.co" link="https://arawato.com.ve/" srcImg="/assets/images/arawato_portfolio.jpg"  />
          </div>
          <div>
          <CardPortfolio imageProject={null} title="Efecto Cocuyo― " description="Plataforma de periodismo hecha por periodistas. Periodismo que ilumina. " page="efectococuyo.com" link="https://efectococuyo.com/" srcImg="/assets/images/efectococuyo_portfolio.jpg" />
          </div>
          <div>
          <CardPortfolio imageProject={null} title="Capitolio― " description="estudio creativo con el propósito de legislar en proyectos de producción audiovisual y diseño gráfico. " page="capitolio.co" link="https://capitolio.co/#/" srcImg="/assets/images/capitolio_portfolio.jpg" />
          </div>
          <div>
          <CardPortfolio imageProject={null} title="VOOY― " description="Vooy! es una app móvil disponible para sistemas Android y IOS que ayuda a los usuarios a descubrir e interconectarse inteligentemente en la vida nocturna. " page="Vooy.app" link="https://vooy.app/" srcImg="/assets/images/vooy_portfolio.jpg" />
          </div>
          <div>
          <CardPortfolio imageProject={null} title="Pasefit― " description="es una app con miles de clases deportivas en un solo pase. Uniendo el mundo deportivo con el digital." page="@pase.fit" link="https://www.instagram.com/pase.fit/?hl=es-la" srcImg="/assets/images/pasefit_portfolio.jpg" />
          </div>
        </Slider>
      </Diver>
    </Diver>
  );
};

/* 
const ImgNext = styled.img`
  width: 15px;
  height: 15px;
`;
const Description = styled.p`
  font-size: 15px;
  color: #999999;
  margin: 0;

  border-radius: 3px;
  text-align: center;

  cursor: pointer;
`;
const WidthDefault = styled.div`
  width: 100%;
  display:flex;
  max-width: 1200px;
  @media (max-width: 1200px) {
    width: 90vw;
  }
`;


const Arrow = styled.div`
display: block;
color: #0047ba;
font-size: 70px;
size: letter;
cursor: pointer;
  @media (max-width: 920px) {
    font-size: 50px;
  }
  @media (max-width: 620px) {
    font-size: 40px;
  }

`;
*/

/* 
const PortfolioCards = styled.div`
  width: 100%;
  max-width: 1200px;
  // min-height: 40vh; //////////////////
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  justify-items: center;
  // background: red; //////////////////
  padding-top: 5vh;

  @media (max-width: 1070px) {
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 40px;
  }
  @media (max-width: 620px) {
    grid-template-columns: 1fr;
  }
`;
*/

export default PortfolioVenture;
